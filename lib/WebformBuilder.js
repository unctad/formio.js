"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es.array.concat");

require("core-js/modules/es.array.every");

require("core-js/modules/es.array.filter");

require("core-js/modules/es.array.find-index");

require("core-js/modules/es.array.for-each");

require("core-js/modules/es.array.includes");

require("core-js/modules/es.array.map");

require("core-js/modules/es.function.name");

require("core-js/modules/es.object.assign");

require("core-js/modules/es.object.get-prototype-of");

require("core-js/modules/es.object.keys");

require("core-js/modules/es.regexp.exec");

require("core-js/modules/es.string.replace");

require("core-js/modules/web.dom-collections.for-each");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Webform2 = _interopRequireDefault(require("./Webform"));

var _dragula = _interopRequireDefault(require("dragula/dist/dragula"));

var _tooltip = _interopRequireDefault(require("tooltip.js"));

var _Components = _interopRequireDefault(require("./components/Components"));

var _builder = _interopRequireDefault(require("./utils/builder"));

var _utils = require("./utils/utils");

var _EventEmitter = _interopRequireDefault(require("./EventEmitter"));

var _nativePromiseOnly = _interopRequireDefault(require("native-promise-only"));

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

require('./components/builder');

var WebformBuilder = /*#__PURE__*/function (_Webform) {
  _inherits(WebformBuilder, _Webform);

  var _super = _createSuper(WebformBuilder);

  function WebformBuilder(element, options) {
    var _this;

    _classCallCheck(this, WebformBuilder);

    _this = _super.call(this, element, options);
    _this.builderHeight = 0;
    _this.dragContainers = [];
    _this.sidebarContainers = [];
    _this.updateDraggable = _lodash.default.debounce(_this.refreshDraggable.bind(_assertThisInitialized(_this)), 200); // Setup the builder options.

    _this.options.builder = _lodash.default.defaultsDeep({}, _this.options.builder, _this.defaultComponents);
    _this.options.enableButtons = _lodash.default.defaults({}, _this.options.enableButtons, {
      remove: true,
      copy: true,
      paste: true,
      edit: true,
      editJson: false
    });
    _this.options.showAdvanced = false;
    _this.options.showHelp = false;
    _this.options.openedPanels = false; // Turn off if explicitely said to do so...

    _lodash.default.each(_this.defaultComponents, function (config, key) {
      if (config === false) {
        _this.options.builder[key] = false;
      }
    });

    _this.builderReady = new _nativePromiseOnly.default(function (resolve) {
      _this.builderReadyResolve = resolve;
    });
    _this.groups = {};
    _this.options.sideBarScroll = _lodash.default.get(_this.options, 'sideBarScroll', true);
    _this.options.sideBarScrollOffset = _lodash.default.get(_this.options, 'sideBarScrollOffset', 0);
    _this.options.hooks = _this.options.hooks || {};

    _this.options.hooks.addComponents = function (components, parent) {
      if (!components || !components.length && !components.nodrop) {
        // Return a simple alert so they know they can add something here.
        return [{
          type: 'htmlelement',
          internal: true,
          tag: 'div',
          className: 'drag-and-drop-alert alert',
          attrs: [{
            attr: 'id',
            value: "".concat(parent.id, "-placeholder")
          }, {
            attr: 'style',
            value: 'text-align:center;'
          }, {
            attr: 'role',
            value: 'alert'
          }],
          content: _this.t('Drag and Drop a form component')
        }];
      }

      return components;
    };

    _this.options.hooks.addComponent = function (container, comp, parent) {
      if (!comp || !comp.component) {
        return container;
      }

      var parentCompReadOnly = parent && parent.schemaReadOnly;
      var compReadOnly = parentCompReadOnly || comp.schemaReadOnly;

      if (!comp.noEdit && !comp.component.internal) {
        // Make sure the component position is relative so the buttons align properly.
        comp.getElement().style.position = 'relative';
        var tooltipPosition = 'top';
        if (comp.component.type === 'panel') tooltipPosition = 'top';
        if (comp.component.type === 'block') tooltipPosition = 'top';
        if (comp.component.type === 'table') tooltipPosition = 'top';
        var removeButton = null;

        if (!parentCompReadOnly) {
          removeButton = _this.ce('div', {
            class: 'btn btn-xxs btn-danger component-settings-button component-settings-button-remove'
          }, _this.getIcon('remove'));

          _this.addEventListener(removeButton, 'click', function () {
            document.dispatchEvent(new CustomEvent('formioDeleteComponent', {
              detail: comp
            }));

            _this.deleteComponent(comp);
          });

          new _tooltip.default(removeButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: removeButton,
            title: _this.t('Remove'),
            popperOptions: {
              positionFixed: true
            }
          });
        }

        var editButton = null;

        if (!compReadOnly) {
          editButton = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button component-settings-button-edit'
          }, _this.getIcon('cog'));

          _this.addEventListener(editButton, 'click', function () {
            document.dispatchEvent(new CustomEvent('formioEditComponent', {
              detail: comp
            }));

            _this.editComponent(comp);
          });

          new _tooltip.default(editButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: editButton,
            title: _this.t('Edit'),
            popperOptions: {
              positionFixed: true
            }
          });
        }

        var moveButton = null;

        if (parent && parent.type === 'datagrid') {
          moveButton = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button component-settings-button-edit'
          }, _this.getIcon('arrows'));
          new _tooltip.default(moveButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: moveButton,
            title: _this.t('Move'),
            popperOptions: {
              positionFixed: true
            }
          });
        }

        var minimizeButton = _this.ce('div', {
          class: 'btn btn-xxs btn-default component-settings-button component-settings-button-minimize'
        }, _this.getIcon('minus'));

        _this.addEventListener(minimizeButton, 'click', function () {
          if (comp.getElement().classList.contains('minimized')) {
            comp.getElement().classList.remove('minimized');
            comp.component.minimized = false;
          } else {
            comp.getElement().classList.add('minimized');
            comp.component.minimized = true;
          }

          if (comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.contains('fa-minus')) {
            comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.remove('fa-minus');
            comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.add('fa-plus');
          } else {
            comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.add('fa-minus');
            comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.remove('fa-plus');
          }

          _this.updateComponent(comp);

          _this.emit('saveComponent', comp);
        });

        new _tooltip.default(minimizeButton, {
          trigger: 'hover',
          placement: tooltipPosition,
          container: minimizeButton,
          title: _this.t('Toggle size'),
          popperOptions: {
            positionFixed: true
          }
        });

        if (comp.component.minimized) {
          comp.getElement().classList.add('minimized');

          if (minimizeButton.querySelector('.fa').classList) {
            minimizeButton.querySelector('.fa').classList.remove('fa-minus');
            minimizeButton.querySelector('.fa').classList.add('fa-plus');
          }
        }

        var copyButton = _this.ce('div', {
          class: 'btn btn-xxs btn-default component-settings-button component-settings-button-copy'
        }, _this.getIcon('copy'));

        _this.addEventListener(copyButton, 'click', function (ev) {
          document.dispatchEvent(new CustomEvent('formioCopyComponent', {
            detail: comp
          }));
          return _this.copyComponent(comp);
        });

        new _tooltip.default(copyButton, {
          trigger: 'hover',
          placement: tooltipPosition,
          container: copyButton,
          title: _this.t('Copy'),
          popperOptions: {
            positionFixed: true
          }
        });
        var pasteButton = null;

        if (!parentCompReadOnly) {
          pasteButton = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button component-settings-button-paste'
          }, _this.getIcon('save'));
          var pasteTooltip = new _tooltip.default(pasteButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: pasteButton,
            title: _this.t('Paste below'),
            popperOptions: {
              positionFixed: true
            }
          });

          _this.addEventListener(pasteButton, 'click', function () {
            pasteTooltip.hide();

            _this.pasteComponent(comp);
          });
        }

        var editJsonButton = null;

        if (!compReadOnly) {
          editJsonButton = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button component-settings-button-edit-json'
          }, _this.getIcon('wrench'));

          _this.addEventListener(editJsonButton, 'click', function () {
            return _this.editComponent(comp, true);
          });

          new _tooltip.default(editJsonButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: editJsonButton,
            title: _this.t('Edit JSON'),
            popperOptions: {
              positionFixed: true
            }
          });
        } // Set in paste mode if we have an item in our clipboard.


        if (window.sessionStorage) {
          var data = window.sessionStorage.getItem('formio.clipboard');

          if (data) {
            _this.addClass(_this.element, 'builder-paste-mode');
          }
        } // Add the edit buttons to the component.

        var fieldActions = _this.ce('div', {
          class: 'component-btn-group component-configuration-group',
          'data-id': comp.component.key
        }, [
            _this.options.enableButtons.edit && (comp.type === 'content' || comp.component.type === 'fieldset' || comp.component.type === 'tabs' || comp.component.type === 'table' || comp.component.type === 'columns' || comp.component.type === 'editgrid' || comp.component.type === 'datagrid') || comp.component.type === 'container' ? minimizeButton : null,
            _this.options.enableButtons.copy ? copyButton : null,
            _this.options.enableButtons.paste ? pasteButton : null,
            _this.options.enableButtons.editJson ? editJsonButton : null,
            _this.options.enableButtons.edit ? editButton : null,
            _this.options.enableButtons.edit ? moveButton : null,
            _this.options.enableButtons.remove ? removeButton : null
          ]);

        
        var children = [];
        if (comp.component.numberOfActions) {
          children.push(comp.component.numberOfActions);
          var hasAction = _this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-action ${comp.component.numberOfActions > 1 ? 'more-than-one' : ''}`
          }, 'A');
          if (!compReadOnly) {
            _this.addEventListener(hasAction, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'action'
              }));
              _this.editComponent(comp, false, 'action');
            });
          }
          new _tooltip.default(hasAction, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasAction,
            title: _this.t('Actions'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasAction);
        }
        if (comp.component.numberOfConditions) {
          children.push(comp.component.numberOfConditions);
          var hasDeterminant = _this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-determinant ${comp.component.numberOfConditions > 1 ? 'more-than-one' : ''}`
          }, 'D');
          if (!compReadOnly) {
            _this.addEventListener(hasDeterminant, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'determinant'
              }));
              _this.editComponent(comp, false, 'determinant');
            });
          }
          new _tooltip.default(hasDeterminant, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasDeterminant,
            title: _this.t('Determinants'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasDeterminant);
        }
        if (comp.component.numberOfFormulas) {
          children.push(comp.component.numberOfFormulas);
          var hasFormula = _this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-formula ${comp.component.numberOfFormulas > 1 ? 'more-than-one' : ''}`
          }, 'F');
          if (!compReadOnly) {
            _this.addEventListener(hasFormula, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'formula'
              }));
              _this.editComponent(comp, false, 'formula');
            });
          }
          new _tooltip.default(hasFormula, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasFormula,
            title: _this.t('Formula'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasFormula);
        }
        if (Array.isArray(comp.component.copyValueFrom) && comp.component.copyValueFrom.length) {
          var hasCopyValueFrom = _this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-copy-value-from ${comp.component.copyValueFrom.length > 1 ? 'more-than-one' : ''}`
          }, 'C');
          if (!compReadOnly) {
            _this.addEventListener(hasCopyValueFrom, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'data'
              }));
              _this.editComponent(comp, false, 'data');
            });
          }
          new _tooltip.default(hasCopyValueFrom, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasCopyValueFrom,
            title: _this.t('CopyValueFrom'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasCopyValueFrom);
        }
        if (comp.component.linkedWithRequirement && comp.component.linkingRequirement || comp.component.linkedWithResult && comp.component.linkingResult) {
          var suffix = comp.component.linkedWithRequirement && comp.component.linkingRequirement ? 'component-configuration-button-linked-requirement' : 'component-configuration-button-linked-result';
          var hasLink = _this.ce('div', {
            class: "btn btn-xxs btn-default component-configuration-button component-configuration-button-linked ".concat(suffix)
          }, 'L');
          if (!compReadOnly) {
            _this.addEventListener(hasLink, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'data'
              }));
              _this.editComponent(comp, false, 'data');
            });
          }
          new _tooltip.default(hasLink, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasLink,
            title: _this.t('Linked'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasLink);
        }
        if (children.length) {
          fieldActions.classList.add('hasIcons');
        }

        comp.prepend(fieldActions);
      }

      if (!container.noDrop && !parentCompReadOnly) {
        _this.addDragContainer(container, parent);
      }

      return container;
    };

    _this.setToolbarElement();

    _this.setBuilderElement();

    return _this;
  }

  _createClass(WebformBuilder, [{
    key: "scrollSidebar",
    value: function scrollSidebar() {
      var newTop = window.scrollY - this.sideBarTop + this.options.sideBarScrollOffset;
      var shouldScroll = newTop > 0;

      if (shouldScroll && newTop + this.sideBarElement.offsetHeight < this.builderHeight) {
        this.sideBarElement.style.marginTop = "".concat(newTop, "px");
      } else if (shouldScroll && this.sideBarElement.offsetHeight < this.builderHeight) {
        this.sideBarElement.style.marginTop = "".concat(this.builderHeight - this.sideBarElement.offsetHeight, "px");
      } else {
        this.sideBarElement.style.marginTop = '0px';
      }
    }
  }, {
    key: "setBuilderElement",
    value: function setBuilderElement() {
      var _this2 = this;

      return this.onElement.then(function () {
        _this2.addClass(_this2.wrapper, 'row formbuilder');

        _this2.builderSidebar = _this2.ce('div', {
          class: 'col col-lg-4 col-xl-3 formcomponents',
          id: 'col1'
        });

        _this2.prependTo(_this2.builderSidebar, _this2.wrapper);

        _this2.addClass(_this2.element, 'formarea');

        _this2.element.component = _this2;
      });
    }
  }, {
    key: "toggleCollapsePanels",
    value: function toggleCollapsePanels() {
      var panelElements = this.element.querySelectorAll('.panel');
      var panelComponents = [];

      for (var i = 0; i < panelElements.length; i++) {
        if (panelElements[i].component) {
          panelComponents.push(panelElements[i].component);
        }
      }

      this.options.openedPanels = panelComponents.filter(function (c) {
        return c.component.title;
      }).every(function (c) {
        return !c.component.collapsed;
      });
      this.togglePanelsTogglerIcon();
    }
  }, {
    key: "togglePanelsTogglerIcon",
    value: function togglePanelsTogglerIcon() {
      if (this.options.openedPanels) {
        this.panelsToggler.getElementsByTagName('i')[0].classList.remove('fa-angle-down');
        this.panelsToggler.getElementsByTagName('i')[0].classList.add('fa-angle-up');
      } else {
        this.panelsToggler.getElementsByTagName('i')[0].classList.add('fa-angle-down');
        this.panelsToggler.getElementsByTagName('i')[0].classList.remove('fa-angle-up');
      }
    }
  }, {
    key: "togglePanels",
    value: function togglePanels() {
      if (this.options.openedPanels) {
        this.emit('goOpenPanels', this);
      } else {
        this.emit('goClosePanels', this);
      }

      this.options.openedPanels = !this.options.openedPanels;
      this.togglePanelsTogglerIcon();
    }
  }, {
    key: "setToolbarElement",
    value: function setToolbarElement() {
      var _this3 = this;

      if (this.options.builder.noToolbar) return;
      this.toolbarRow = document.getElementById('myToolbar'); //const toolbarHtml = require('./htmltemplates/formbuilder-toolbar.html');

      var t1 = this.t('Containers');
      var t2 = this.t('Toggle blocks');
      var t3 = this.t('Fields');
      var t4 = this.t('Other components');
      var t5 = this.t('Data');
      var t6 = this.t('Custom components');
      var t7 = this.t('Service forms');
      var toolbarHtml = "<div class=\"formbuilder-toolbar row\"><div class=\"col-auto formio-component formio-component-tabs formbuilder-toolbar-fixed-container col p-0\"><ul class=\"nav nav-tabs justify-content-between\" id=\"formbuilder-toolbar-fixed-nav\" role=\"tablist\"><li class=\"nav-item\"><a class=\"nav-link active\" id=\"tools-containers-link\" data-toggle=\"tab\" data-target=\"#tools-containers\" role=\"tab\" aria-controls=\"tools-containers\" aria-selected=\"true\">".concat(t1, "</a></li><li class=\"pull-right nav-item\"><a id=\"panelsToggler\" class=\"nav-link\"><i class=\"fa fa-angle-down\"></i>").concat(t2, "</a></li></ul><div class=\"tab-content\" id=\"formbuilder-toolbar-fixed-content\"><div class=\"tab-pane show active\" id=\"tools-containers\" role=\"tabpanel\" aria-labelledby=\"tools-containers-link\"><div class=\"row\"><div id=\"toolbar-layout-components\" class=\"col\"></div></div></div></div></div><div class=\"formio-component formio-component-tabs col formbuilder-toolbar-container p-0\"><ul class=\"nav nav-tabs\" id=\"formbuilder-toolbar-nav\" role=\"tablist\"><li class=\"nav-item\"><a class=\"nav-link active\" id=\"tools-fields-link\" data-toggle=\"tab\" data-target=\"#tools-fields\" role=\"tab\" aria-controls=\"tools-fields\" aria-selected=\"true\">").concat(t3, "</a></li><li class=\"nav-item\"><a class=\"nav-link\" id=\"tools-others-link\" data-target=\"#tools-others\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"tools-others\" aria-selected=\"false\">").concat(t4, "</a></li><li class=\"nav-item\"><a class=\"nav-link\" id=\"tools-data-link\" data-toggle=\"tab\" data-target=\"#tools-data\" role=\"tab\" aria-controls=\"tools-data\" aria-selected=\"false\">").concat(t5, "</a></li><li class=\"nav-item\"><a class=\"nav-link\" id=\"tools-custom-link\" data-toggle=\"tab\" data-target=\"#tools-custom\" role=\"tab\" aria-controls=\"tools-custom\" aria-selected=\"false\">").concat(t6, "</a></li><li class=\"nav-item\"><a class=\"nav-link\" id=\"tools-serviceforms-link\" data-toggle=\"tab\" data-target=\"#tools-serviceforms\" role=\"tab\" aria-controls=\"tools-serviceforms\" aria-selected=\"false\">").concat(t7, "</a></li></ul><div class=\"tab-content\" id=\"formbuilder-toolbar-content\"><div class=\"tab-pane fade show active\" id=\"tools-fields\" role=\"tabpanel\" aria-labelledby=\"tools-fields-link\"><div class=\"row\"><div id=\"toolbar-basic-components\" class=\"col\"></div></div></div><div class=\"tab-pane fade\" id=\"tools-others\" role=\"tabpanel\" aria-labelledby=\"tools-others-link\"><div class=\"row\"><div id=\"toolbar-advanced-components\" class=\"col\"></div></div></div><div class=\"tab-pane fade\" id=\"tools-data\" role=\"tabpanel\" aria-labelledby=\"tools-data-link\"><div class=\"row\"><div id=\"toolbar-data-components\" class=\"col\"></div></div></div><div class=\"tab-pane fade\" id=\"tools-custom\" role=\"tabpanel\" aria-labelledby=\"tools-custom-link\"><div class=\"row\"><div id=\"toolbar-custom-components\" class=\"col\"></div></div></div><div class=\"tab-pane fade\" id=\"tools-serviceforms\" role=\"tabpanel\" aria-labelledby=\"tools-custom-link\"><div class=\"row\"><div id=\"toolbar-serviceforms-components\" class=\"col\"></div></div></div></div></div></div>");
      this.toolbarRow.innerHTML = toolbarHtml;
      this.groupBasic = this.toolbarRow.querySelector('#toolbar-basic-components');
      this.groupAdvanced = this.toolbarRow.querySelector('#toolbar-advanced-components');
      this.groupLayout = this.toolbarRow.querySelector('#toolbar-layout-components');
      this.groupData = this.toolbarRow.querySelector('#toolbar-data-components');
      this.groupCustom = this.toolbarRow.querySelector('#toolbar-custom-components');
      this.groupServiceForms = this.toolbarRow.querySelector('#toolbar-serviceforms-components');
      this.panelsToggler = this.toolbarRow.querySelector('#panelsToggler');
      this.panelsToggler.addEventListener('click', function (event) {
        event.preventDefault();

        _this3.togglePanels();
      });

      var allComponents = _lodash.default.filter(_lodash.default.map(_Components.default.components, function (component, type) {
        if (!component.builderInfo || component.builderInfo.hidden) {
          return null;
        }

        component.type = type;
        return component;
      }));

      if (this.options.builder.customComponents && this.options.builder.customComponents.components) {
        Array.prototype.push.apply(allComponents, this.options.builder.customComponents.components);
      }

      var components = {};

      _lodash.default.map(_lodash.default.sortBy(allComponents, function (component) {
        return component.builderInfo.weight;
      }), function (component) {
        var builderInfo = component.builderInfo;
        builderInfo.key = component.type;
        components[builderInfo.key] = builderInfo;
      });

      _lodash.default.each(components, function (component) {
        component.element = _this3.ce('span', _defineProperty({
          id: "builder-".concat(component.key),
          class: "btn formcomponent drag-copy ".concat(component.extraClass)
        }, 'aria-label', component.title));

        if (component.group === 'serviceforms') {
          component.element.addEventListener('click', function (ev) {
            document.dispatchEvent(new CustomEvent('openFormForCopy', {
              detail: component
            }));
            ev.preventDefault();
          });
        }

        component.tooltip = new _tooltip.default(component.element, {
          trigger: 'hover',
          placement: 'bottom',
          container: component.element,
          title: _this3.t(component.title)
        });

        if (component.group !== 'serviceforms') {
          _this3.addEventListener(component.element, 'mouseout', function () {
            return component.tooltip.hide();
          });
        }

        if (component.icon) {
          component.element.appendChild(_this3.ce('i', {
            class: component.icon,
            'aria-hidden': 'true'
          }));
        }

        component.element.builderInfo = component;

        if (component.group === 'basic') {
          _this3.groupBasic.appendChild(component.element);
        } else if (component.group === 'advanced') {
          _this3.groupAdvanced.appendChild(component.element);
        } else if (component.group === 'layout') {
          _this3.groupLayout.appendChild(component.element);
        } else if (component.group === 'data') {
          _this3.groupData.appendChild(component.element);
        } else if (component.group === 'custom') {
          _this3.groupCustom.appendChild(component.element);
        } else if (component.group === 'serviceforms') {
          _this3.groupServiceForms.appendChild(component.element);
        }
      });

      this.groupBasic.component = this;
      this.groupAdvanced.component = this;
      this.groupLayout.component = this;
      this.groupData.component = this;
      this.groupCustom.component = this;
      this.groupServiceForms.component = this;
      this.updateDraggable();
      this.on('aPanelCollapsed', function (comp) {
        _this3.emit('saveComponent', comp);

        _this3.toggleCollapsePanels();
      });
      this.on('allPanelsCollapsed', function (comp) {
        _this3.emit('saveComponent', comp);
      });
      this.toggleCollapsePanels();
    }
  }, {
    key: "setForm",
    value: function setForm(form) {
      var _this4 = this;

      //populate isEnabled for recaptcha form settings
      var isRecaptchaEnabled = false;

      if (form.components) {
        (0, _utils.eachComponent)(form.components, function (component) {
          if (isRecaptchaEnabled) {
            return;
          }

          if (component.type === 'recaptcha') {
            isRecaptchaEnabled = true;
            return false;
          }
        });

        if (isRecaptchaEnabled) {
          _lodash.default.set(form, 'settings.recaptcha.isEnabled', true);
        } else if (_lodash.default.get(form, 'settings.recaptcha.isEnabled')) {
          _lodash.default.set(form, 'settings.recaptcha.isEnabled', false);
        }
      }

      this.emit('change', form);
      return _get(_getPrototypeOf(WebformBuilder.prototype), "setForm", this).call(this, form).then(function (retVal) {
        setTimeout(function () {
          return _this4.builderHeight = _this4.element.offsetHeight;
        }, 200);
        return retVal;
      });
    }
  }, {
    key: "deleteComponent",
    value: function deleteComponent(component) {
      if (!component.parent) {
        return;
      }

      if (component.component && component.component.canBeRemoved === false) {
        return;
      }

      var remove = true;

      if (typeof component.getComponents === 'function' && component.getComponents().length > 0) {
        var message = 'Removing this component will also remove all of its children. Are you sure you want to do this?';
        remove = window.confirm(this.t(message));
      }

      if (remove) {
        this.emit('deleteComponentId', component.id);
        component.parent.removeComponentById(component.id);
        this.form = this.schema;
        this.emit('deleteComponent', component);
      }

      return remove;
    }
  }, {
    key: "updateComponent",
    value: function updateComponent(component) {
      var _this5 = this;

      // Update the preview.
      if (this.componentPreview) {
        if (this.preview) {
          this.preview.destroy();
        }

        this.preview = _Components.default.create(component.component, {
          preview: true,
          events: new _EventEmitter.default({
            wildcard: false,
            maxListeners: 0
          })
        }, {}, true);
        this.preview.on('componentEdit', function (comp) {
          _lodash.default.merge(component.component, comp.component);

          _this5.editForm.redraw();
        });
        this.preview.build();
        this.preview.isBuilt = true;
        this.componentPreview.innerHTML = '';
        this.componentPreview.appendChild(this.preview.getElement());
      } // Ensure this component has a key.


      if (component.isNew) {
        var namespaceKey = this.recurseNamespace(component.parent);

        if (!component.keyModified) {
          if (!namespaceKey && this.options.formName) {
            // if component is not inside any container and exists form prefix then lets add it
            component.component.key = _lodash.default.camelCase(this.options.formName.concat(component.component.label || component.component.placeholder || component.component.type));
          } else {
            component.component.key = _lodash.default.camelCase(component.component.label || component.component.placeholder || component.component.type);
          }
        } // Set a unique key for this component.
        // BuilderUtils.uniquify(this._form, component.component);


        var ns = this.findNamespaceRoot(component.component); // if (!namespaceKey) {

        _builder.default.uniquify(this._form, component.component); // }
        // else {
        //   BuilderUtils.uniquify(ns, component.component);
        // }

      } // Change the "default value" field to be reflective of this component.


      if (this.defaultValueComponent) {
        _lodash.default.assign(this.defaultValueComponent, _lodash.default.omit(component.component, ['key', 'label', 'placeholder', 'tooltip', 'validate', 'disabled', 'fields.day.required', 'fields.month.required', 'fields.year.required']));
      } // Called when we update a component.


      this.emit('updateComponent', component);
    }
    /**
       * When a component sets its api key, we need to check if it is unique within its namespace. Find the namespace root
       * so we can calculate this correctly.
       * @param component
       */

  }, {
    key: "findNamespaceRoot",
    value: function findNamespaceRoot(component) {
      // First get the component with nested parents.
      var comp = (0, _utils.getComponent)(component.parent ? component.parent.components : this._form.components, component.key, true);
      var namespaceKey = this.recurseNamespace(comp); // If there is no key, it is the root form.

      if (!namespaceKey || this.form.key === namespaceKey) {
        return this.form.components;
      } // If the current component is the namespace, we don't need to find it again.


      if (namespaceKey === component.key) {
        return component.components;
      } // Get the namespace component so we have the original object.


      var namespaceComponent = (0, _utils.getComponent)(this.form.components, namespaceKey, true);
      return namespaceComponent.components;
    }
  }, {
    key: "recurseNamespace",
    value: function recurseNamespace(component) {
      // If there is no parent, we are at the root level.
      if (!component) {
        return null;
      } // Some components are their own namespace.


      if (['tree'].includes(component.type) || component.tree || component.arrayTree) {
        return component.key;
      } // Anything else, keep going up.


      return this.recurseNamespace(component.parent);
    }
  }, {
    key: "toggleAdvancedFields",
    value: function toggleAdvancedFields() {
      if (this.dialog) {
        this.advancedFields = this.dialog.getElementsByClassName('advanced-field');

        if (this.options.showAdvanced) {
          this.dialog.advancedToggler.getElementsByTagName('i')[0].classList.remove('fa-flip-horizontal', 'text-muted');
          Array.prototype.filter.call(this.advancedFields, function (advancedField) {
            if (advancedField.parentNode.classList.contains('col')) {
              advancedField.parentNode.classList.remove('hidden');
            } else {
              advancedField.classList.remove('hidden');
            }
          });
          this.dialog.body.classList.add('advanced-field-enabled');
        } else {
          this.dialog.advancedToggler.getElementsByTagName('i')[0].classList.add('fa-flip-horizontal', 'text-muted');
          Array.prototype.filter.call(this.advancedFields, function (advancedField) {
            if (advancedField.parentNode.classList.contains('col')) {
              advancedField.parentNode.classList.add('hidden');
            } else {
              advancedField.classList.add('hidden');
            }
          });
          this.dialog.body.classList.remove('advanced-field-enabled');
        }
      }
    }
  }, {
    key: "toggleHelp",
    value: function toggleHelp() {
      if (this.dialog) {
        this.helpFields = this.dialog.getElementsByClassName('fa-question-circle');

        if (this.options.showHelp) {
          this.dialog.helpToggler.getElementsByTagName('i')[0].classList.remove('fa-flip-horizontal', 'text-muted');
          Array.prototype.filter.call(this.helpFields, function (helpField) {
            helpField.classList.remove('hidden');
          });
          this.dialog.body.classList.add('help-tooltip-enabled');
        } else {
          this.dialog.helpToggler.getElementsByTagName('i')[0].classList.add('fa-flip-horizontal', 'text-muted');
          Array.prototype.filter.call(this.helpFields, function (helpField) {
            helpField.classList.add('hidden');
          });
          this.dialog.body.classList.remove('help-tooltip-enabled');
        }
      }
    }
    /* eslint-disable max-statements */

  }, {
    key: "editComponent",
    value: function editComponent(component, isJsonEdit) {
      var _this6 = this;

      var tabKey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      this.toggleAdvancedFields();
      this.toggleHelp();

      var componentCopy = _lodash.default.cloneDeep(component);

      var componentClass = _Components.default.components[componentCopy.component.type];
      var isCustom = componentClass === undefined; //custom component should be edited as JSON

      isJsonEdit = isJsonEdit || isCustom;
      componentClass = isCustom ? _Components.default.components.unknown : componentClass; // Make sure we only have one dialog open at a time.

      if (this.dialog) {
        this.dialog.close();
      }

      this.dialog = this.createModal(componentCopy.name);
      var formioForm = this.ce('div');
      this.componentPreview = this.ce('div', {
        class: 'component-preview'
      });
      var componentInfo = componentClass ? componentClass.builderInfo : {};
      var saveButton = this.ce('button', {
        class: 'btn btn-success',
        style: 'margin-right: 10px;',
        'data-event': 'save-component'
      }, this.t('general.save'));
      var topSaveButton = saveButton.cloneNode(true);
      var cancelButton = this.ce('button', {
        class: 'btn btn-default',
        style: 'margin-right: 10px;'
      }, this.t('general.cancel'));
      var topCancelButton = cancelButton.cloneNode(true);
      var removeButton = this.ce('button', {
        class: 'btn btn-danger'
      }, this.t('Remove'));
      var topRemoveButton = removeButton.cloneNode(true);
      this.dialog.helpToggler = this.ce('span', {
        class: 'modal-help-toggler'
      }, [this.ce('i', {
        class: 'fa fa-toggle-on fa-flip-horizontal toggle__icon ml-4 mr-2'
      }), this.t('Help')]);
      this.dialog.advancedToggler = this.ce('span', {
        class: 'modal-advanced-toggler'
      }, [this.ce('i', {
        class: 'fa fa-toggle-on fa-flip-horizontal toggle__icon ml-4 mr-2'
      }), this.t('Advanced')]);
      this.addEventListener(this.dialog.helpToggler, 'click', function (event) {
        event.preventDefault();
        _this6.options.showHelp = !_this6.options.showHelp;

        _this6.toggleHelp();
      });
      this.addEventListener(this.dialog.advancedToggler, 'click', function (event) {
        event.preventDefault();
        _this6.options.showAdvanced = !_this6.options.showAdvanced;

        _this6.toggleAdvancedFields();
      });
      var componentTitle = componentCopy.component.subType ? componentCopy.component.subType : componentInfo.title;
      var componentEdit = this.ce('div', {}, [this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'col'
      }, this.ce('h1', {
        class: 'page-title'
      }, [component.isNew ? "Add ".concat(componentTitle) : "Edit ".concat(componentTitle), this.dialog.helpToggler, this.dialog.advancedToggler]))]), this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'col col-sm-12 mb-2'
      }, [topSaveButton, topCancelButton, topRemoveButton])]), this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'col col-sm-12'
      }, formioForm)]), this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'col col-sm-12 mt-2'
      }, [saveButton, cancelButton, removeButton])])]); // Append the settings page to the dialog body.

      this.dialog.body.appendChild(componentEdit); // Allow editForm overrides per component.

      var overrides = _lodash.default.get(this.options, "editForm.".concat(componentCopy.component.type), {}); // Get the editform for this component.


      var editForm; //custom component has its own Edit Form defined

      if (isJsonEdit && !isCustom) {
        editForm = {
          'components': [{
            'type': 'textarea',
            'as': 'json',
            'editor': 'ace',
            'weight': 10,
            'input': true,
            'key': 'componentJson',
            'label': 'Component JSON',
            'tooltip': 'Edit the JSON for this component.'
          }]
        };
      } else {
        editForm = componentClass.editForm(_lodash.default.cloneDeep(overrides));
      } // Change the defaultValue component to be reflective.


      this.defaultValueComponent = (0, _utils.getComponent)(editForm.components, 'defaultValue');

      _lodash.default.assign(this.defaultValueComponent, _lodash.default.omit(componentCopy.component, ['key', 'label', 'placeholder', 'tooltip', 'validate', 'disabled', 'fields.day.required', 'fields.month.required', 'fields.year.required'])); // Create the form instance.


      var editFormOptions = _lodash.default.get(this, 'options.editForm', {});

      this.editForm = new _Webform2.default(formioForm, _objectSpread({
        language: this.options.language
      }, editFormOptions));
      if (component.isNew) this.editForm.element.classList.add('is-new'); // Set the form to the edit form.

      this.editForm.form = editForm; // Pass along the form being edited.

      this.editForm.editForm = this._form;
      this.editForm.editComponent = component; // Update the preview with this component.

      this.updateComponent(componentCopy); // Register for when the edit form changes.

      this.editForm.on('change', function (event) {
        _this6.toggleAdvancedFields();

        _this6.toggleHelp();

        if (event.changed) {
          // See if this is a manually modified key. Treat JSON edited component keys as manually modified
          if (event.changed.component && event.changed.component.key === 'key' || isJsonEdit) {
            componentCopy.keyModified = true;
          } // Set the component JSON to the new data.


          var editFormData = _this6.editForm.getValue().data; //for custom component use value in 'componentJson' field as JSON of component


          if ((editFormData.type === 'custom' || isJsonEdit) && editFormData.componentJson) {
            componentCopy.component = editFormData.componentJson;
          } else {
            componentCopy.component = editFormData;
          } // Update the component.


          _this6.updateComponent(componentCopy);
        }
      }); // Modify the component information in the edit form.

      this.editForm.formReady.then(function () {
        //for custom component populate component setting with component JSON
        if (isJsonEdit) {
          _this6.editForm.setValue({
            data: {
              componentJson: _lodash.default.cloneDeep(componentCopy.component)
            }
          });
        } else {
          _this6.editForm.setValue({
            data: componentCopy.component
          });

          if (tabKey) {
            var tabsComp = _this6.editForm.getComponent('tabs');

            if (tabsComp && tabsComp.component && Array.isArray(tabsComp.component.components)) {
              // eslint-disable-next-line max-depth
              for (var i = 0; i < tabsComp.component.components.length; i++) {
                // eslint-disable-next-line max-depth
                if (tabsComp.component.components[i].key === tabKey) {
                  tabsComp.setTab(i);
                  break;
                }
              }
            }
          }
        }

        _this6.toggleAdvancedFields();

        _this6.toggleHelp();
      });
      this.addEventListener(topCancelButton, 'click', function (event) {
        event.preventDefault();

        _this6.emit('cancelComponent', component);

        _this6.dialog.close();
      });
      this.addEventListener(cancelButton, 'click', function (event) {
        event.preventDefault();

        _this6.emit('cancelComponent', component);

        _this6.dialog.close();
      });
      this.addEventListener(topRemoveButton, 'click', function (event) {
        event.preventDefault();

        _this6.deleteComponent(component);

        _this6.dialog.close();
      });
      this.addEventListener(removeButton, 'click', function (event) {
        event.preventDefault();

        _this6.deleteComponent(component);

        _this6.dialog.close();
      });
      this.addEventListener(saveButton, 'click', function (event) {
        event.preventDefault();

        if (!_this6.editForm.checkValidity(_this6.editForm.data, true)) {
          console.log('Form data is not valid! Cancelling saving!', _this6.editForm.data);
          var errors = _this6.editForm.errors;
          console.log('Validation errors', errors);

          _this6.emit('validationErrors', errors, component);

          return;
        }

        var originalComponent = component.schema;
        component.isNew = false; //for JSON Edit use value in 'componentJson' field as JSON of component

        if (isJsonEdit) {
          component.component = _this6.editForm.data.componentJson;
        } else {
          component.component = componentCopy.component;
        }

        if (component.dragEvents && component.dragEvents.onSave) {
          component.dragEvents.onSave(component);
        }

        _this6.form = _this6.schema;

        _this6.emit('saveElement', _this6.editForm.data);

        _this6.emit('saveComponent', component, originalComponent);

        _this6.dialog.close();
      });
      this.addEventListener(topSaveButton, 'click', function (event) {
        event.preventDefault();

        if (!_this6.editForm.checkValidity(_this6.editForm.data, true)) {
          console.log('Form data is not valid! Cancelling saving!', _this6.editForm.data);
          var errors = _this6.editForm.errors;
          console.log('Validation errors', errors);

          _this6.emit('validationErrors', errors, component);

          return;
        }

        var originalComponent = component.component;
        component.isNew = false; //for JSON Edit use value in 'componentJson' field as JSON of component

        if (isJsonEdit) {
          component.component = _this6.editForm.data.componentJson;
        } else {
          component.component = componentCopy.component;
        }

        if (component.dragEvents && component.dragEvents.onSave) {
          component.dragEvents.onSave(component);
        }

        _this6.form = _this6.schema;

        _this6.emit('saveElement', _this6.editForm.data);

        _this6.emit('saveComponent', component, originalComponent);

        _this6.dialog.close();
      });
      this.addEventListener(this.dialog, 'close', function () {
        _this6.editForm.destroy(true);

        _this6.preview.destroy(true);

        _this6.dialog.remove();

        if (component.isNew) {
          _this6.deleteComponent(component);
        }
      }); // Called when we edit a component.

      this.emit('editComponent', component);
    }
    /* eslint-enable max-statements */

    /**
     * Creates copy of component schema and stores it under sessionStorage.
     * @param {Component} component
     * @return {*}
     */

  }, {
    key: "copyComponent",
    value: function copyComponent(component) {
      if (!window.sessionStorage) {
        return console.log('Session storage is not supported in this browser.');
      }

      this.addClass(this.element, 'builder-paste-mode');

      var copy = _lodash.default.cloneDeep(component.schema);

      window.sessionStorage.setItem('formio.clipboard', JSON.stringify(copy));
    }
    /**
     * Paste copied component after the current component.
     * @param {Component} component
     * @return {*}
     */

  }, {
    key: "pasteComponent",
    value: function pasteComponent(component) {
      if (!window.sessionStorage) {
        return console.log('Session storage is not supported in this browser.');
      }

      this.removeClass(this.element, 'builder-paste-mode');
      var data = window.sessionStorage.getItem('formio.clipboard');

      if (data) {
        var schema = JSON.parse(data);
        window.sessionStorage.removeItem('formio.clipboard');

        _builder.default.uniquify(this._form, schema); // If this is an empty "nested" component, and it is empty, then paste the component inside this component.


        if (typeof component.addComponent === 'function' && !component.components.length) {
          component.addComponent(schema);
        } else {
          component.parent.addComponent(schema, false, false, component.element.nextSibling);
        }

        this.form = this.schema;
      }
    }
  }, {
    key: "destroy",
    value: function destroy() {
      var state = _get(_getPrototypeOf(WebformBuilder.prototype), "destroy", this).call(this);

      if (this.dragula) {
        this.dragula.destroy();
      }

      return state;
    }
    /**
     * Insert an element in the weight order.
     *
     * @param info
     * @param items
     * @param element
     * @param container
     */

  }, {
    key: "insertInOrder",
    value: function insertInOrder(info, items, element, container) {
      // Determine where this item should be added.
      var beforeWeight = 0;
      var before = null;

      _lodash.default.each(items, function (itemInfo) {
        if (info.key !== itemInfo.key && info.weight < itemInfo.weight && (!beforeWeight || itemInfo.weight < beforeWeight)) {
          before = itemInfo.element;
          beforeWeight = itemInfo.weight;
        }
      });

      if (before) {
        try {
          container.insertBefore(element, before);
        } catch (err) {
          container.appendChild(element);
        }
      } else {
        container.appendChild(element);
      }
    }
  }, {
    key: "getFieldIcon",
    value: function getFieldIcon(type) {
      if (type) {
        switch (type.toLowerCase()) {
          case 'panel':
          case 'block':
            return 'far fa-window';

          case 'editgrid':
          case 'datagrid':
            return 'far fa-th';

          case 'table':
            return 'far fa-table';

          case null:
          default:
            return false;
        }
      } else {
        return false;
      }
    }
  }, {
    key: "addBuilderGroup",
    value: function addBuilderGroup(info, container) {
      var _this7 = this;

      var fromAsyncContext = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      if (!info || !info.key) {
        console.warn('Invalid Group Provided.');
        return;
      }

      info = _lodash.default.clone(info);
      var groupAnchor = this.ce('button', {
        class: 'btn btn-block builder-group-button',
        'type': 'button',
        'data-toggle': 'collapse',
        'data-parent': "#".concat(container.id),
        'data-target': "#group-".concat(info.key)
      }, [this.getFieldIcon(info.type) ? this.ce('i', {
        class: "".concat(this.getFieldIcon(info.type), " branch-bloc-icon mr-2")
      }, '') : '', this.text(info.title)]); // Add a listener when it is clicked.

      if (!(0, _utils.bootstrapVersion)(this.options)) {
        this.addEventListener(groupAnchor, 'click', function (event) {
          event.preventDefault();
          var clickedGroupId = event.target.getAttribute('data-target').replace('#group-', '');

          if (_this7.groups[clickedGroupId]) {
            var clickedGroup = _this7.groups[clickedGroupId];

            var wasIn = _this7.hasClass(clickedGroup.panel, 'in');

            _lodash.default.each(_this7.groups, function (group, groupId) {
              _this7.removeClass(group.panel, 'in');

              _this7.removeClass(group.panel, 'show');

              if (groupId === clickedGroupId && !wasIn) {
                _this7.addClass(group.panel, 'in');

                _this7.addClass(group.panel, 'show');

                var parent = group.parent;

                while (parent) {
                  _this7.addClass(parent.panel, 'in');

                  _this7.addClass(parent.panel, 'show');

                  parent = parent.parent;
                }
              }
            }); // Match the form builder height to the sidebar.


            _this7.element.style.minHeight = "".concat(_this7.builderSidebar.offsetHeight, "px");

            _this7.scrollSidebar();
          }
        }, true);
      }

      info.element = this.ce('div', {
        class: 'card panel panel-default form-builder-panel',
        id: "group-panel-".concat(info.key)
      }, [this.ce('div', {
        class: 'card-header panel-heading form-builder-group-header'
      }, [this.ce('h5', {
        class: 'mb-0 panel-title'
      }, groupAnchor)])]);
      info.body = this.ce('div', {
        id: "group-container-".concat(info.key),
        class: 'card-body panel-body no-drop'
      }); // Add this group body to the drag containers.

      this.sidebarContainers.push(info.body);
      var groupBodyClass = 'panel-collapse collapse';

      if (info.default) {
        switch ((0, _utils.bootstrapVersion)(this.options)) {
          case 4:
            groupBodyClass += ' show';
            break;

          case 3:
            groupBodyClass += ' in';
            break;

          default:
            groupBodyClass += ' in show';
            break;
        }
      }

      info.panel = this.ce('div', {
        class: groupBodyClass,
        'data-parent': "#".concat(container.id),
        id: "group-".concat(info.key)
      }, info.body);
      info.element.appendChild(info.panel);
      this.groups[info.key] = info;
      this.insertInOrder(info, this.groups, info.element, container);

      var cb = function cb() {
        var asyncContext = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

        // Now see if this group has subgroups.
        if (info.groups) {
          _lodash.default.each(info.groups, function (subInfo, subGroup) {
            subInfo.key = subGroup;
            subInfo.parent = info;

            _this7.addBuilderGroup(subInfo, info.body, asyncContext);
          });
        }

        if (asyncContext) {
          _lodash.default.each(info.components, function (comp, key) {
            if (comp) {
              _this7.addBuilderComponent(comp, info);
            }
          });

          if ((!info.components || !Object.keys(info.components).length) && (!info.groups || !Object.keys(info.groups).length)) {
            _this7.addBuilderComponent({
              key: 'empty',
              title: _this7.t('No choices to choose from'),
              noOptions: true
            }, info);
          }
        }
      };

      if (info.loadOnDemand) {
        var loadComponents = function loadComponents(loadedInfo) {
          info.alreadyLoaded = true;
          Object.assign(info, loadedInfo);
          _this7.groups[info.key] = info; // eslint-disable-next-line callback-return

          cb(true);
          groupAnchor.classList.remove('preload');

          _this7.updateDraggable();
        };

        this.addEventListener(groupAnchor, 'click', function (event) {
          event.preventDefault();
          if (info.alreadyLoaded) return;
          groupAnchor.classList.add('preload');
          return info.loadOnDemand(loadComponents);
        }, true);
      } else {
        return cb(fromAsyncContext);
      }
    }
  }, {
    key: "addBuilderComponentInfo",
    value: function addBuilderComponentInfo(component) {
      if (!component || !component.group || !this.groups[component.group]) {
        return;
      }

      component = _lodash.default.clone(component);
      var groupInfo = this.groups[component.group];

      if (!groupInfo.components) {
        groupInfo.components = {};
      }

      if (!groupInfo.components.hasOwnProperty(component.key)) {
        groupInfo.components[component.key] = component;
      }

      return component;
    }
  }, {
    key: "addBuilderComponent",
    value: function addBuilderComponent(component, group) {
      if (!component) {
        return;
      }

      if (!group && component.group && this.groups[component.group]) {
        group = this.groups[component.group];
      }

      if (!group) {
        return;
      }

      component.element = this.ce('span', {
        id: "builder-".concat(component.key),
        class: 'btn btn-primary btn-xs btn-block formcomponent drag-copy'
      });

      if (component.icon) {
        component.element.appendChild(this.ce('i', {
          class: "".concat(component.icon, " mr2")
        }));
      }

      var copyTo = this.ce('span', {
        class: component.noOptions ? '' : 'copyMoustach far fa-brackets-curly'
      });
      component.element.appendChild(copyTo);

      if (!component.noOptions) {
        this.addEventListener(copyTo, 'click', function () {
          var copyText = component.type === 'select' ? "{{data.".concat(component.key, ".value}}") : "{{data.".concat(component.key, "}}");
          navigator.clipboard.writeText(copyText);
        }, true);
      }

      component.element.builderInfo = component;
      var elementLabel = this.ce('span', {
        class: 'btn-label'
      });
      elementLabel.appendChild(this.text(component.title));
      component.element.appendChild(elementLabel);
      this.insertInOrder(component, group.components, component.element, group.body);
      return component;
    }
  }, {
    key: "addBuilderButton",
    value: function addBuilderButton(info, container) {
      var _this8 = this;

      var button;
      info.element = this.ce('div', {
        style: 'margin: 5px 0;'
      }, button = this.ce('span', {
        class: "btn btn-block ".concat(info.style || 'btn-default')
      }, info.title)); // Make sure it persists across refreshes.

      this.addEventListener(button, 'click', function () {
        return _this8.emit(info.event);
      }, true);
      this.groups[info.key] = info;
      this.insertInOrder(info, this.groups, info.element, container);
    }
  }, {
    key: "buildSidebar",
    value: function buildSidebar() {
      var _this9 = this;

      // Do not rebuild the sidebar.
      if (this.sideBarElement || this.options.builder.noSideBar) {
        return;
      }

      this.groups = {};
      this.sidebarContainers = [];
      this.sideBarElement = this.ce('div', {
        id: "builder-sidebar-".concat(this.id),
        class: 'accordion panel-group'
      }); // Add the groups.

      _lodash.default.each(this.options.builder, function (info, group) {
        if (info) {
          info.key = group;

          if (info.type === 'button') {
            _this9.addBuilderButton(info, _this9.sideBarElement);
          } else {
            _this9.addBuilderGroup(info, _this9.sideBarElement);
          }
        }
      }); // Get all of the components builder info grouped and sorted.


      var components = {};

      var allComponents = _lodash.default.filter(_lodash.default.map(_Components.default.components, function (component, type) {
        if (!component.builderInfo) {
          return null;
        }

        component.type = type;
        return component;
      }));

      _lodash.default.map(_lodash.default.sortBy(allComponents, function (component) {
        return component.builderInfo.weight;
      }), function (component) {
        var builderInfo = component.builderInfo;
        builderInfo.key = component.type;
        components[builderInfo.key] = builderInfo;

        _this9.addBuilderComponentInfo(builderInfo);
      }); // Add the components in each group.


      _lodash.default.each(this.groups, function (info) {
        if (info.loadOnDemand) return;

        _lodash.default.each(info.components, function (comp, key) {
          if (comp) {
            _this9.addBuilderComponent(comp === true ? components[key] : comp, info);
          }
        });
      }); // Add the new sidebar element.


      this.builderSidebar.appendChild(this.sideBarElement);
      this.updateDraggable();
      this.sideBarTop = this.sideBarElement.getBoundingClientRect().top + window.scrollY;
    }
  }, {
    key: "getParentElement",
    value: function getParentElement(element) {
      var containerComponent = element;

      do {
        containerComponent = containerComponent.parentNode;
      } while (containerComponent && !containerComponent.component);

      return containerComponent;
    }
  }, {
    key: "addDragContainer",
    value: function addDragContainer(element, component, dragEvents) {
      _lodash.default.remove(this.dragContainers, function (container) {
        return element.id && element.id === container.id;
      });

      element.component = component;

      if (dragEvents) {
        element.dragEvents = dragEvents;
      }

      this.addClass(element, 'drag-container');

      if (!element.id) {
        element.id = "builder-element-".concat(component.id);
      }

      this.dragContainers.push(element);
      this.updateDraggable();
    }
  }, {
    key: "clear",
    value: function clear() {
      this.dragContainers = [];
      return _get(_getPrototypeOf(WebformBuilder.prototype), "clear", this).call(this);
    }
  }, {
    key: "addComponentTo",
    value: function addComponentTo(schema, parent, element, sibling, after) {
      var component = parent.addComponent(schema, element, parent.data, sibling);

      if (after) {
        after(component);
      } // Get path to the component in the parent component.


      var path = 'components';

      switch (component.parent.type) {
        case 'table':
          path = "rows[".concat(component.tableRow, "][").concat(component.tableColumn, "].components");
          break;

        case 'columns':
          path = "columns[".concat(component.column, "].components");
          break;

        case 'tabs':
          path = "components[".concat(component.tab, "].components");
          break;
      } // Index within container


      var index = _lodash.default.findIndex(_lodash.default.get(component.parent.schema, path), {
        key: component.component.key
      }) || 0;
      this.emit('addComponent', component, path, index);
      return component;
    }
    /* eslint-disable  max-statements */

  }, {
    key: "onDrop",
    value: function onDrop(element, target, source, sibling) {
      if (!element || !element.id) {
        console.warn('No element.id defined for dropping');
        return;
      }

      var builderElement = source.querySelector("#".concat(element.id));
      var newParent = this.getParentElement(element);

      if (!newParent || !newParent.component) {
        return console.warn('Could not find parent component.');
      } // Remove any instances of the placeholder.


      var placeholder = document.getElementById("".concat(newParent.component.id, "-placeholder"));

      if (placeholder) {
        placeholder = placeholder.parentNode;
        placeholder.parentNode.removeChild(placeholder);
      } // If the sibling is the placeholder, then set it to null.


      if (sibling === placeholder) {
        sibling = null;
      } // Make this element go before the submit button if it is still on the builder.


      if (!sibling && this.submitButton && newParent.contains(this.submitButton.element)) {
        sibling = this.submitButton.element;
      } // If this is a new component, it will come from the builderElement


      if (builderElement && builderElement.builderInfo && !builderElement.builderInfo.schema && builderElement.builderInfo.builderInfo) {
        builderElement.builderInfo.schema = builderElement.builderInfo.builderInfo.schema;
      }

      if (builderElement && builderElement.builderInfo && builderElement.builderInfo.schema) {
        var componentSchema = _lodash.default.clone(builderElement.builderInfo.schema);

        if (target.dragEvents && target.dragEvents.onDrop) {
          target.dragEvents.onDrop(element, target, source, sibling, componentSchema);
        } // Add the new component.


        var component = this.addComponentTo(componentSchema, newParent.component, newParent, sibling, function (comp) {
          // Set that this is a new component.
          comp.isNew = true; // Pass along the save event.

          if (target.dragEvents) {
            comp.dragEvents = target.dragEvents;
          }
        }); // Edit the component.

        this.editComponent(component); // Remove the element.

        target.removeChild(element);
      } // Check to see if this is a moved component.
      else if (element.component) {
          var _componentSchema = element.component.schema;

          if (target.dragEvents && target.dragEvents.onDrop) {
            target.dragEvents.onDrop(element, target, source, sibling, _componentSchema);
          } // Remove the component from its parent.


          if (element.component.parent) {
            this.emit('deleteComponent', element.component);
            element.component.parent.removeComponent(element.component);
          } // Add the new component.


          var _component = this.addComponentTo(_componentSchema, newParent.component, newParent, sibling);

          if (target.dragEvents && target.dragEvents.onSave) {
            target.dragEvents.onSave(_component);
          } // Refresh the form.


          if (target.component && target.component.root && target.component.root !== this) {
            target.component.root.form = target.component.root.schema;
          }

          this.form = this.schema;
          this.emit('saveComponent', _component);
        }
    }
    /* eslint-enable  max-statements */

    /**
     * Adds a submit button if there are no components.
     */

  }, {
    key: "addSubmitButton",
    value: function addSubmitButton() {
      if (!this.getComponents().length) {
        this.submitButton = this.addComponent({
          type: 'button',
          label: this.t('Submit'),
          key: 'submit',
          size: 'md',
          block: false,
          action: 'submit',
          disableOnInvalid: true,
          theme: 'primary'
        });
      }
    }
  }, {
    key: "refreshDraggable",
    value: function refreshDraggable() {
      var _this10 = this;

      if (this.dragula) {
        this.dragula.destroy();
      }

      var dragables = this.sidebarContainers.concat(this.dragContainers).concat(this.groupBasic).concat(this.groupAdvanced).concat(this.groupLayout).concat(this.groupData).concat(this.groupCustom).concat(this.groupServiceForms);

      if (this.options.externalDragables) {
        dragables = dragables.concat(this.options.externalDragables);
      }

      var that = this;
      dragables.forEach(function (dragable) {
        if (!dragable) return;
        dragable.addEventListener('click', function (ev) {
          that.cancelDragAndDrop = true;
        });
      });
      this.dragula = (0, _dragula.default)(dragables, {
        moves: function moves(el) {
          if (el.classList.contains('no-drag')) {
            return false;
          }

          setTimeout(function () {
            if (that.cancelDragAndDrop) {
              that.cancelDragAndDrop = false;
              return;
            }

            document.dispatchEvent(new CustomEvent('formioFormBuilderDragStarted', {
              detail: that
            }));
          }, 300);
          return true;
        },
        copy: function copy(el) {
          return el.classList.contains('drag-copy');
        },
        accepts: function accepts(el, target) {
          return !el.contains(target) && !target.classList.contains('no-drop');
        }
      }).on('drop', function (element, target, source, sibling) {
        var result = _this10.onDrop(element, target, source, sibling);

        document.dispatchEvent(new CustomEvent('formioFormBuilderDragEnded', {
          detail: _this10
        }));
        return result;
      }); // If there are no components, then we need to add a default submit button.
      //this.addSubmitButton();

      this.builderReadyResolve();
    }
  }, {
    key: "build",
    value: function build(state) {
      this.buildSidebar();

      _get(_getPrototypeOf(WebformBuilder.prototype), "build", this).call(this, state);

      this.updateDraggable();
      this.formReadyResolve();
    }
  }, {
    key: "defaultComponents",
    get: function get() {
      return {
        basic: {
          title: 'Basic Components',
          weight: 0,
          default: true
        },
        advanced: {
          title: 'Advanced',
          weight: 10
        },
        layout: {
          title: 'Layout',
          weight: 20
        },
        data: {
          title: 'Data',
          weight: 30
        }
      };
    }
  }, {
    key: "ready",
    get: function get() {
      return this.builderReady;
    }
  }]);

  return WebformBuilder;
}(_Webform2.default);

exports.default = WebformBuilder;