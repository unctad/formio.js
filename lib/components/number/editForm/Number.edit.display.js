"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = [{
  key: 'prefixSufixRow',
  type: 'columns',
  weight: 300,
  components: [{
    type: 'column',
    components: [{
      type: 'textfield',
      input: true,
      weight: 310,
      key: 'prefix',
      label: 'prefix',
      tooltip: 'Specify the prefix symbol after the component (e.g.: USD, EUR)'
    }]
  }, {
    type: 'column',
    components: [{
      type: 'textfield',
      input: true,
      weight: 320,
      key: 'suffix',
      label: 'suffix',
      tooltip: 'Specify the suffix symbol after the component (e.g.: USD, EUR).'
    }]
  }]
}, {
  'label': '',
  'columns': [{
    'components': [{
      weight: 400,
      type: 'textfield',
      input: true,
      key: 'errorLabel',
      label: 'Error Label',
      placeholder: 'Error Label',
      customClass: 'advanced-field',
      tooltip: 'The label for this field when an error occurs.'
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      weight: 600,
      type: 'textfield',
      input: true,
      key: 'tabindex',
      label: 'Tab Index',
      placeholder: 'Tab Index',
      customClass: 'advanced-field',
      tooltip: 'Sets the tabindex attribute of this component to override the tab order of the form. See the <a href=\\\'https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex\\\'>MDN documentation</a> on tabindex for more information.'
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-5'
}, {
  key: 'column-3',
  ignore: true
}];
exports.default = _default;