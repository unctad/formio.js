"use strict";

require("core-js/modules/es.symbol");

require("core-js/modules/es.symbol.description");

require("core-js/modules/es.symbol.iterator");

require("core-js/modules/es.array.for-each");

require("core-js/modules/es.array.iterator");

require("core-js/modules/es.object.keys");

require("core-js/modules/es.object.to-string");

require("core-js/modules/es.string.iterator");

require("core-js/modules/web.dom-collections.for-each");

require("core-js/modules/web.dom-collections.iterator");

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Components", {
  enumerable: true,
  get: function get() {
    return _Components.default;
  }
});
Object.defineProperty(exports, "Formio", {
  enumerable: true,
  get: function get() {
    return _Formio.default;
  }
});
Object.defineProperty(exports, "Form", {
  enumerable: true,
  get: function get() {
    return _Form2.default;
  }
});
Object.defineProperty(exports, "Utils", {
  enumerable: true,
  get: function get() {
    return _utils.default;
  }
});

var _lodash = _interopRequireDefault(require("lodash"));

var _components = _interopRequireDefault(require("./components"));

var _Components = _interopRequireDefault(require("./components/Components"));

var _Formio = _interopRequireDefault(require("./Formio"));

var _Form2 = _interopRequireDefault(require("./Form"));

var _utils = _interopRequireDefault(require("./utils"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

_lodash.default.noConflict();

_Components.default.setComponents(_components.default);

var registerPlugin = function registerPlugin(plugin) {
  // Sanity check.
  if (_typeof(plugin) !== 'object') {
    return;
  }

  for (var _i = 0, _Object$keys = Object.keys(plugin); _i < _Object$keys.length; _i++) {
    var key = _Object$keys[_i];

    switch (key) {
      case 'options':
        _Formio.default.options = _lodash.default.merge(_Formio.default.options, plugin.options);
        break;

      default:
        console.log('Unknown plugin option', key);
    }
  }
};
/**
 * Allows passing in plugins as multiple arguments or an array of plugins.
 *
 * Formio.plugins(plugin1, plugin2, etc);
 * Formio.plugins([plugin1, plugin2, etc]);
 */


_Formio.default.use = function () {
  for (var _len = arguments.length, plugins = new Array(_len), _key = 0; _key < _len; _key++) {
    plugins[_key] = arguments[_key];
  }

  plugins.forEach(function (plugin) {
    if (Array.isArray(plugin)) {
      plugin.forEach(function (p) {
        return registerPlugin(p);
      });
    } else {
      registerPlugin(plugin);
    }
  });
};

_Formio.default.Components = _Components.default;