import _ from 'lodash';
import { eachComponent, isExtendedLayoutComponent, uniqueKey } from './utils';
export default {
  /**
   * Appends a number to a component.key to keep it unique
   *
   * @param {Object} form
   *   The components parent form.
   * @param {Object} component
   *   The component to uniquify
   * @param prefix
   * @param emptyRelations
   * @param transform
   * @param idMapping
   */
  uniquify(form, component, prefix, emptyRelations, transform, idMapping) {
    const formKeys = {};
    const oldToNew = {};
    const deletionArray = [];
    let editgridPresent = false;
    if (typeof prefix !== 'string') {
      prefix = '';
    }
    eachComponent(form.components, function(comp) {
      formKeys[comp.key] = true;
    }, true);

    // Recurse into all child components.
    // eslint-disable-next-line max-statements
    eachComponent([component], (component, newPath, components, index)  => {
      // Skip key uniquification if this component doesn't have a key.
      if (!component.key) {
        return;
      }

      if (emptyRelations) {
        if (!transform) {
          delete component.determinantIds;
          delete component.formDeterminantId;
          delete component.effectsIds;
          delete component.behaviourId;
          delete component.tabItemIds;
          delete component.componentTabId;
        }
        delete component.formulaRowIds;
        delete component.actionRowIds;
        delete component.validationRowIds;
        delete component.blacklistRowIds;

        delete component.componentFormulaId;
        delete component.componentActionId;
        delete component.componentValidationId;

        delete component.numberOfConditions;
        delete component.numberOfRelatedDeterminants;
        delete component.numberOfFormulas;
        delete component.numberOfActions;
        delete component.numberOfRegistrations;

        delete component.copyValueFrom;
        delete component.registrations;
        delete component.linkedWithRequirement;
        delete component.linkingRequirement;
        delete component.linkedWithResult;
        delete component.linkingResult;
        delete component.serviceId;
        delete component.dynamicValidations;
      }

      if (transform) {
        if (component.type === 'button' || component.type === 'file') {
          // delete the component object
          deletionArray.push({
            array: components,
            index: index
          });
          return;
        }
        if (component.type === 'content' && !(component.html?.includes('{{') && component.html?.includes('}}'))) {
          // delete the component object
          deletionArray.push({
            array: components,
            index: index
          });
          return;
        }
        if (component.type === 'catalog' || component.type === 'select') {
          component.copyValueFrom = [component.key];
          component.validate = {};
          component.dataSrc = 'none';
          component.template = '<span className="value">{{t(item.value)}}</span>';
          delete component.catalog;
          delete component.catalogueDefaultValue;
          delete component.defaultValue;
          delete component.parentCatalogField;
        }
        else if (!isExtendedLayoutComponent(component)) {
          component.copyValueFrom = [component.key];
          component.validate = {};
        }
        else if (component.type === 'editgrid') {
          component.copyValueFrom = [component.key];
          component.disableAddingRows = true;
          component.disableRemovingRows = true;
          component.validate = {};
          editgridPresent = true;
        }
        else if (component.type === 'datagrid') {
          component.copyValueFrom = [component.key];
          component.disableAddingRows = true;
          component.disableRemovingRows = true;
          component.validate = {};
        }
        else if (component.type === 'tabs') {
          component.type = 'panel';
        }
        else if ((!component.type || component.type === 'tab') && Array.isArray(component.components)) {
          if (component.condition > 0 && idMapping) {
            component.formDeterminantId = idMapping[component.id].id;
            component.determinantIds = [...idMapping[component.id].determinantIds];
          }
          component.type = 'panel';
          component.title = component.label;
          delete component.label;
          delete component.condition;
        }
        if (component.type === 'panel') {
          component.collapsibleDS = false;
          component.collapsedDS = false;
          component.collapsed = false;
          delete component.tabItemIds;
          delete component.componentTabId;
        }
      }

      const oldKey = component.key;
      component.key = _.camelCase(prefix.concat(
        component.label ||
        component.placeholder ||
        component.type
      ));

      const newKey = uniqueKey(formKeys, component.key, prefix);
      if (oldKey !== newKey) {
        oldToNew[oldKey] = newKey;
        component.key = newKey;
        formKeys[newKey] = true;
      }
      else {
        component.key = oldKey;
      }
    }, true);
    deletionArray.reverse().forEach((obj) => {
      obj.array.splice(obj.index, 1);
    });

    if (editgridPresent) {
      eachComponent([component], (comp) => {
        if (comp.type !== 'editgrid') return;
        const fieldsShowInGridArray = [];
        const totalColumnsArray = [];
        comp.fieldsShownInGrid?.forEach(field => {
          fieldsShowInGridArray.push(oldToNew[field]);
        });
        comp.fieldsShownInGrid = fieldsShowInGridArray;
        comp.totalColumns?.forEach(field => {
          totalColumnsArray.push(oldToNew[field]);
        });
        comp.totalColumns = totalColumnsArray;
        if (comp.percentageColumn) comp.percentageColumn = oldToNew[comp.percentageColumn];
      }, true);
    }
    return oldToNew;
  },

  additionalShortcuts: {
    button: [
      'Enter',
      'Esc'
    ]
  },

  extractNecessaryProperties(obj) {
    const necessaryProps = ['components', 'columns', 'rows', 'type', 'key', 'formDeterminantId'];

    if (Array.isArray(obj)) {
      return obj.map(item => this.extractNecessaryProperties(item));
    }
    else if (typeof obj === 'object' && obj !== null) {
      const newObj = {};
      for (const prop in obj) {
        if (necessaryProps.includes(prop)) {
          newObj[prop] = this.extractNecessaryProperties(obj[prop]);
        }
      }
      return newObj;
    }
    else {
      return obj;
    }
  },

  getAlphaShortcuts() {
    return _.range('A'.charCodeAt(), 'Z'.charCodeAt() + 1).map((charCode) => String.fromCharCode(charCode));
  },

  getAdditionalShortcuts(type) {
    return this.additionalShortcuts[type] || [];
  },

  getBindedShortcuts(components, input) {
    const result = [];

    eachComponent(components, function(component) {
      if (component === input) {
        return;
      }

      if (component.shortcut) {
        result.push(component.shortcut);
      }
      if (component.values) {
        component.values.forEach((value) => {
          if (value.shortcut) {
            result.push(value.shortcut);
          }
        });
      }
    }, true);

    return result;
  },

  getAvailableShortcuts(form, component) {
    if (!component) {
      return [];
    }
    return [''].concat(_.difference(
      this.getAlphaShortcuts().concat(this.getAdditionalShortcuts(component.type)),
      this.getBindedShortcuts(form.components, component))
    );
  }
};
