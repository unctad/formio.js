import BaseComponent from '../base/Base';

export default class ContentComponent extends BaseComponent {
  static schema(...extend) {
    return BaseComponent.schema({
      label: 'Content',
      type: 'content',
      key: 'content',
      input: false,
      html: '',
      hidden: false,
      minimized: false
    }, ...extend);
  }

  static get builderInfo() {
    return {
      title: 'Content',
      group: 'basic',
      icon: 'fab fa-html5',
      documentation: 'http://help.form.io/userguide/#content-component',
      weight: 71,
      schema: ContentComponent.schema(),
    };
  }

  get defaultSchema() {
    return ContentComponent.schema();
  }

  setHTML() {
    this.htmlElement.innerHTML = this.interpolate(this.component.html);
  }

  build() {
    this.createElement();
    this.htmlElement = this.ce('div', {
      id: this.id,
      class: `form-group ${this.component.className}`
    });
    if (this.component.showLabel) {
      this.labelElement = this.ce('label', {
        class: 'control-label'
      });
      this.labelSpan = this.ce('span');
      this.labelElement.appendChild(this.labelSpan);
    }
    this.htmlElement.component = this;

    if (this.options.builder) {
      if (this.component.showLabel) {
        this.labelSpan.appendChild(this.generateLabelElement(this.component.label, this.component.workingLabel));
      }
      this.element.classList.add('content-builder');
      this.htmlElement.classList.add('ck-content');
      if (!this.schemaReadOnly) {
        this.element.addEventListener('dblclick', this.editHtml.bind(this));
      }
      this.htmlElement.innerHTML = this.component.html;
    }
    else {
      if (this.component.showLabel) {
        this.labelSpan.appendChild(this.text(this.component.label));
      }
      this.setHTML();
      if (this.component.refreshOnChange) {
        this.on('change', () => this.setHTML(), true);
      }
    }
    if (this.component.showLabel) {
      this.createTooltip(this.labelElement);
      this.element.appendChild(this.labelElement);
    }
    this.element.appendChild(this.htmlElement);
    this.attachLogic();
  }

  get emptyValue() {
    return '';
  }

  editHtml(e) {
    if (this.editorReady) {
      return;
    }
    this.htmlElement.innerHTML = '';
    const ckConfig = this.options.ckConfig || {};
    const editorElement = this.ce('div');
    this.element.appendChild(editorElement);
    let oldHtml = this.component.html;
    this.editorReady = this.addCKE(editorElement, ckConfig, (html) => {
      this.component.html = html;
      if (this.component.html && this.component.html.includes('{{') && this.component.html.includes('}}')) {
        this.component.refreshOnChange = true;
      }
    }).then((editor) => {
      this.editor = editor;
      editor.ui.focusTracker.on( 'change:isFocused', ( evt, name, isFocused ) => {
        if ( isFocused ) {
          oldHtml = this.component.html;
        }
        else {
          document.dispatchEvent(new CustomEvent('formioCKBlur', {
            detail: {
              component: this,
              oldHtml
            }
          }));
        }
      } );
      this.editor.data.set(this.component.html);
      return editor;
    }).catch(err => console.warn(err));
  }

  destroy() {
    const state = super.destroy();
    if (
      this.options.builder && this.editor &&
      this.element && this.element.parentNode && this.editorReady
    ) {
      this.editor.destroy();
      this.editorReady = null;
    }
    return state;
  }
}
