import NestedComponent from '../nested/NestedComponent';

export default class PanelComponent extends NestedComponent {
  static schema(...extend) {
    return NestedComponent.schema({
      label: 'Block',
      type: 'panel',
      key: 'panel',
      title: '',
      theme: 'default',
      breadcrumb: 'default',
      components: [],
      clearOnHide: false,
      input: false,
      tableView: false,
      dataGridLabel: true,
      persistent: false,
      lazyLoad: false,
      collapsible: true,
      collapsed: false
    }, ...extend);
  }

  static get builderInfo() {
    return {
      title: 'Block',
      icon: 'fa fa-list-alt',
      group: 'layout',
      documentation: 'http://help.form.io/userguide/#panels',
      weight: 1,
      schema: PanelComponent.schema()
    };
  }

  constructor(component, options, data) {
    super(component, options, data);
    this.lazyLoaded = false;
  }

  get defaultSchema() {
    return PanelComponent.schema();
  }

  getContainer() {
    return this.panelBody;
  }

  get className() {
    return `panel panel-${this.component.theme} ${super.className}`;
  }

  getCollapseIcon() {
    const collapseIcon = this.getIcon(this.collapsed ? 'angle-down' : 'angle-up');
    this.addClass(collapseIcon, 'formio-collapse-icon');
    return collapseIcon;
  }

  setPanelBodyDefaultClass() {
    return 'card-body panel-body panel-collapse collapse ';
  }

  getCollapsedClass() {
    const collapseClass = this.collapsed ? '' : 'show';
    return this.setPanelBodyDefaultClass() + collapseClass;
  }

  setCollapsed(element) {
    super.setCollapsed(element);
    if (this.collapseIcon) {
      const newIcon = this.getCollapseIcon();
      this.panelTitle.replaceChild(newIcon, this.collapseIcon);
      this.collapseIcon = newIcon;
      this.panelBody.className = this.getCollapsedClass();
    }
  }

  /**
   * Return if this panel is lazy loadable.
   * @return {boolean}
   */
  get lazyLoadable() {
    return !this.options.builder &&
      this.component.lazyLoad &&
      this.component.collapsible &&
      this.collapsed &&
      !this.lazyLoaded;
  }

  checkValidity(data, dirty) {
    // Make sure to toggle the collapsed state before checking validity.
    if (dirty && this.lazyLoadable) {
      this.lazyLoaded = true;
      this.addComponents();
    }

    return super.checkValidity(data, dirty);
  }

  addComponents(element, data, options, state) {
    // If they are lazy loading, then only add the components if they toggle the collapsed state.
    if (this.lazyLoadable) {
      return;
    }
    return super.addComponents(element, data, options, state);
  }

  toggleCollapse(toState) {
    console.log('toggleCollapse');
    if (toState !== undefined && toState !== null) {
      if (toState === !this.collapsed) {
        super.toggleCollapse('ok');
        this.component.collapsed = this.collapsed;
        this.emit('allPanelsCollapsed', this);
      }
    }
    else {
      if (this.lazyLoadable) {
        this.lazyLoaded = true;
        this.addComponents();
      }
      super.toggleCollapse(null);
      //this.collapsed = !this.collapsed;
      this.component.collapsed = this.collapsed;
      this.emit('aPanelCollapsed', this);
    }
  }

  build(state) {
    this.component.theme = this.component.theme || 'default';
    let panelClass = 'mb-2 card border ';
    panelClass += `panel panel-${this.component.theme} `;
    panelClass += this.component.customClass;
    if (this.component.hidden) {
      panelClass += ' hide';
    }
    if (!this.component.title && this.options.builder) {
      panelClass += ' card-no-header ';
    }
    this.element = this.ce('div', {
      id: this.id,
      class: panelClass
    });
    this.element.component = this;
    this.panelBody = this.ce('div', {
      class: 'card-body panel-body'
    });
    if (this.component.title && (this.options.builder || !this.component.hideLabel)) {
      const heading = this.ce('div', {
        class: `card-header bg-${this.component.theme} panel-heading`
      });
      this.panelTitle = this.ce('h4', {
        class: 'mb-0 card-title panel-title'
      });
      if (this.component.hideLabel) {
        this.panelTitle.classList.add('hidden-label');
      }
      if (this.component.collapsible) {
        this.collapseIcon = this.getCollapseIcon();
        this.panelTitle.appendChild(this.collapseIcon);
        this.panelTitle.appendChild(this.text(' '));
      }
      if (this.options.builder) {
        this.panelTitle.appendChild(this.generateLabelElement(this.component.title, this.component.workingTitle));
      }
      else {
        this.panelTitle.appendChild(this.text(this.component.title));
      }
      this.createTooltip(this.panelTitle);
      heading.appendChild(this.panelTitle);
      this.setCollapseHeader(heading);
      this.element.appendChild(heading);
    }
    else {
      this.createTooltip(this.panelBody, this.component, `${this.iconClass('question-sign')} text-muted formio-hide-label-panel-tooltip`);
    }

    this.addComponents(null, null, null, state);
    this.element.appendChild(this.panelBody);
    this.setCollapsed();
    this.attachLogic();

    this.on('goOpenPanels', () => {
      this.toggleCollapse(true);
    });
    this.on('goClosePanels', () => {
      this.toggleCollapse(false);
    });
  }
  onAnyChanged() {
    if (this.options.builder && this.panelTitle && this.panelTitle.children[0]) {
        this.panelTitle.innerHTML = '';
        if (this.component.collapsible) {
            this.collapseIcon = this.getCollapseIcon();
            this.panelTitle.appendChild(this.collapseIcon);
            this.panelTitle.appendChild(this.text(' '));
        }
        if (this.options.builder) {
            this.panelTitle.appendChild(this.generateLabelElement(this.component.title, this.component.workingTitle));
        }
        else {
            this.panelTitle.appendChild(this.text(this.component.title));
        }
        this.createTooltip(this.panelTitle);
    }
  }
}
