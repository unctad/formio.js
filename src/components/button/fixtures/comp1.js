export default {
  'type': 'button',
  'theme': 'primary',
  'disableOnInvalid': false,
  'action': 'event',
  'block': false,
  'rightIcon': '',
  'leftIcon': '',
  'size': 'md',
  'key': 'submit',
  'tableView': false,
  'label': 'Submit',
  'input': true
};
