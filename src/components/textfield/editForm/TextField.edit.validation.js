export default [
  {
    weight: 110,
    key: 'validate.minLength',
    label: 'Minimum Length',
    placeholder: 'Minimum Length',
    type: 'number',
    tooltip: 'The minimum length requirement this field must meet.',
    input: true
  },
  {
    weight: 120,
    key: 'validate.maxLength',
    label: 'Maximum Length',
    placeholder: 'Maximum Length',
    type: 'number',
    tooltip: 'The maximum length requirement this field must meet.',
    input: true
  },
  {
    weight: 125,
    key: 'validate.minWords',
    label: 'Minimum Word Length',
    placeholder: 'Minimum Word Length',
    type: 'number',
    tooltip: 'The minimum amount of words that can be added to this field.',
    input: true
  },
  {
    weight: 126,
    key: 'validate.maxWords',
    label: 'Maximum Word Length',
    placeholder: 'Maximum Word Length',
    type: 'number',
    tooltip: 'The maximum amount of words that can be added to this field.',
    input: true
  },
  {
    weight: 130,
    key: 'validate.pattern',
    label: 'Regular Expression Pattern',
    placeholder: 'Regular Expression Pattern',
    type: 'textfield',
    tooltip: 'The regular expression pattern test that the field value must pass before the form can be submitted.',
    input: true,
    customClass: 'advanced-field'
  },
  {
    'label': '',
    'columns': [
      {
        'components': [
          {
            weight: 410,
            type: 'textfield',
            input: true,
            key: 'inputMask',
            label: 'Input Mask',
            tooltip: 'An input mask helps the user with input by ensuring a predefined format.<br><br>9: numeric<br>a: alphabetical<br>*: alphanumeric<br><br>Example telephone mask: (999) 999-9999<br><br>See the <a target=\'_blank\' href=\'https://github.com/RobinHerbots/jquery.inputmask\'>jquery.inputmask documentation</a> for more information.</a>',
            customConditional: 'show = !data.allowMultipleMasks;'
          }
        ],
        'width': 4,
        'offset': 0,
        'push': 0,
        'pull': 0,
        'type': 'column',
        'input': false,
        'hideOnChildrenHidden': false,
        'key': 'column-1',
        'tableView': true,
        'label': 'Column'
      },
      {
        'components': [
          {
            weight: 413,
            type: 'checkbox',
            input: true,
            key: 'allowMultipleMasks',
            label: 'Allow Multiple Masks'
          }
        ],
        'width': 3,
        'offset': 0,
        'push': 0,
        'pull': 0,
        'type': 'column',
        'input': false,
        'hideOnChildrenHidden': false,
        'key': 'column-1',
        'tableView': true,
        'label': 'Column'
      },
      {
        'components': [
          {
            weight: 400,
            type: 'textfield',
            input: true,
            key: 'errorLabel',
            label: 'Error Label',
            placeholder: 'Error Label',
            customClass: 'advanced-field',
            tooltip: 'The label for this field when an error occurs.'
          }
        ],
        'width': 5,
        'offset': 0,
        'push': 0,
        'pull': 0,
        'type': 'column',
        'input': false,
        'hideOnChildrenHidden': false,
        'key': 'column-1',
        'tableView': true,
        'label': 'Column'
      }
    ],
    'mask': false,
    'tableView': false,
    'alwaysEnabled': false,
    'type': 'columns',
    'input': false,
    'key': 'column-2'
  },
  {
    weight: 417,
    type: 'datagrid',
    input: true,
    key: 'inputMasks',
    label: 'Input Masks',
    customConditional: 'show = data.allowMultipleMasks === true;',
    reorder: true,
    components: [
      {
        type: 'textfield',
        key: 'label',
        label: 'Label',
        input: true
      },
      {
        type: 'textfield',
        key: 'mask',
        label: 'Mask',
        input: true
      }
    ]
  },
];
