import baseEditForm from '../base/Base.form';

import EmailEditDisplay from './editForm/Email.edit.display';
export default function(...extend) {
  return baseEditForm([
    {
      key: 'display',
      components: EmailEditDisplay
    }
  ], ...extend);
}
