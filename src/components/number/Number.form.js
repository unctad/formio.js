import baseEditForm from '../base/Base.form';

import NumberEditDisplay from './editForm/Number.edit.display';
import NumberEditData from './editForm/Number.edit.data';
import NumberEditValidation from './editForm/Number.edit.validation';

export default function(...extend) {
  return baseEditForm([
    {
      key: 'display',
      components: NumberEditDisplay
    },
    {
      key: 'data',
      components: NumberEditData
    },
    {
      key: 'validation',
      components: NumberEditValidation
    }
  ], ...extend);
}
