import NestedComponent from '../nested/NestedComponent';
import Base from '../base/Base';
import _ from 'lodash';
import Tooltip from 'tooltip.js';

export default class TabsComponent extends NestedComponent {
  static schema(...extend) {
    return NestedComponent.schema({
      label: 'Tabs',
      type: 'tabs',
      input: false,
      key: 'tabs',
      persistent: false,
      components: [
        {
          label: 'Tab 1',
          key: 'tab0',
          components: [],
        },
      ],
    }, ...extend);
  }

  static get builderInfo() {
    return {
      title: 'Tabs',
      group: 'layout',
      icon: 'fal fa-folder-tree',
      weight: 50,
      documentation: 'http://help.form.io/userguide/#tabs',
      schema: TabsComponent.schema()
    };
  }

  constructor(component, options, data) {
    super(component, options, data);
    this.currentTab = 0;
    this.validityTabs = [];
  }

  get defaultSchema() {
    return TabsComponent.schema();
  }

  onAnyChanged() {
    const tabLabels = {};
    for (const tab of this.component.components) {
      if (tab.label && tab.label.includes('{{') && tab.label.includes('}}')) {
        if (!this.options.builder || !tab.workingLabel) {
          tabLabels[tab.key] = tab.label;
        }
      }
    }
    const tabHeader = this.element.querySelector(':scope > ul.nav.nav-tabs');
    const tabLinks = tabHeader.querySelectorAll(':scope > li > a');
    for (let i = 0; i < tabLinks.length; i++) {
      const key = tabLinks[i].href.split('#')[1];
      if (key && tabLabels[key]) {
        if (this.options.builder) {
          tabLinks[i].replaceChild(this.generateLabelElement(tabLabels[key]), tabLinks[i].children[0]);
        }
        else {
          tabLinks[i].innerText = this.interpolate(tabLabels[key]);
        }
      }
    }
  }

  get schema() {
    const schema = super.schema;
    let currentTab = null;
    if (this.currentTabComp) {
      let currentTabComponent = this.component.components.find(c => c.id && c.id === this.currentTabComp.id);
      if (!currentTabComponent) {
        currentTabComponent = this.component.components.find(c => c.key && c.key === this.currentTabComp.key);
      }
      if (currentTabComponent) {
        currentTab = this.component.components.indexOf(currentTabComponent);
      }
    }
    if (currentTab == null && Array.isArray(this.components) && this.components.length > 0) {
      const childComp = this.components[0];
      let index = null;
      for (let i = 0; i < this.component.components.length; i++) {
        // eslint-disable-next-line max-depth
        if (this.component.components[i].components) {
          const childComponent = this.component.components[i].components.find(c => c.id === childComp.id);
          // eslint-disable-next-line max-depth
          if (childComponent) {
            index = i;
            break;
          }
        }
      }
      if (index != null) {
        currentTab = index;
      }
    }

    schema.components = this.component.components.map((tab, index) => {
      if (index === currentTab) {
        tab.components = this.getComponents().map((component) => component.schema);
      }

      return tab;
    });

    if (Number.isSafeInteger(currentTab)) {
      this.currentTab = currentTab;
    }

    return schema;
  }

  build(state, showLabel) {
    if (this.options.flatten) {
      this.element = super.createElement();
      this.component.components.forEach((tab) => {
        let body;
        const panel = this.ce('div', {
          id: this.id,
          class: 'mb-2 card border panel panel-default'
        },
          [
            this.ce('div', {
              class: 'card-header bg-default panel-heading'
            },
              this.ce('h4', {
                class: 'mb-0 card-title panel-title'
              }, tab.label)
            ),
            body = this.ce('div', {
              class: 'card-body panel-body'
            })
          ]
        );
        tab.components.forEach(component => this.addComponent(
          component,
          body,
          this.data,
          null,
          null,
          this.getComponentState(component, state)
        ));
        this.element.appendChild(panel);
      });
    }
    else {
      return super.build(state, showLabel);
    }
  }

  createElement() {
    this.tabsBar = this.ce('ul', {
      class: 'nav nav-tabs nav-pill mb-4',
    });
    this.tabsContent = this.ce('div', {
      class: 'tab-content',
    });

    this.tabLinks = [];
    this.tabs = [];
    this.component.components.forEach((tab, index) => {
      let tabLabel = tab.label;
      const anchor = [];
      if (this.options?.builder && tab.workingLabel) {
        anchor.push(this.generateLabelElement(tabLabel || '', tab.workingLabel));
      }
      else if (tabLabel) {
        if (tabLabel.includes('{{') && tabLabel.includes('}}')) {
          if (this.options?.builder) {
            anchor.push(this.generateLabelElement(tabLabel));
          }
          else {
            tabLabel = this.interpolate(tabLabel);
          }
        }
        if (anchor.length === 0) {
          anchor.push(tabLabel);
        }
      }
      if (this.options?.builder && tab.condition) {
        const hasDeterminant = this.ce('span', {
          class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-determinant ${tab.condition > 1 ? 'more-than-one' : ''}`
        }, 'D');
        this.root.addEventListener(hasDeterminant, 'click', (e) => {
          e.preventDefault();
          e.stopPropagation();
          document.dispatchEvent(new CustomEvent('formioEditComponent', { detail: this, tab: 'tabTabs' }));
          this.root.editComponent(this, false, 'tabTabs');
        });
        new Tooltip(hasDeterminant, {
          trigger: 'hover',
          placement: 'top',
          container: hasDeterminant,
          title: this.t('Determinants'),
          popperOptions: { positionFixed: true }
        });
        anchor.push(hasDeterminant);
      }

      const tabLink = this.ce('a', {
        class: 'nav-link',
        href: `#${tab.key}`,
      }, anchor);
      if (tab.hidden) {
        tabLink.classList.add('hide');
      }
      this.addEventListener(tabLink, 'click', (event) => {
        event.preventDefault();
        this.setTab(index);
      });
      const tabElement = this.ce('li', {
        class: 'nav-item',
        role: 'presentation',
      }, tabLink);
      tabElement.tabLink = tabLink;
      this.tabsBar.appendChild(tabElement);
      this.tabLinks.push(tabElement);

      const tabPanel = this.ce('div', {
        role: 'tabpanel',
        class: 'tab-pane',
        id: tab.key,
      });
      this.tabsContent.appendChild(tabPanel);
      this.tabs.push(tabPanel);
    });

    if (this.element) {
      this.appendChild(this.element, [this.tabsBar, this.tabsContent]);
      this.element.className = this.className;
      return this.element;
    }

    this.element = this.ce('div', {
      id: this.id,
      class: this.className,
    }, [this.tabsBar, this.tabsContent]);
    this.element.component = this;

    return this.element;
  }

  /**
   * Set the current tab.
   *
   * @param index
   */
  setTab(index, state) {
    if (
      !this.tabs ||
      !this.component.components ||
      !this.component.components[this.currentTab] ||
      (this.currentTab >= this.tabs.length)
    ) {
      return;
    }

    this.currentTab = index;

    // Get the current tab.
    const tab = this.component.components[index];
    this.currentTabComp = tab;
    this.empty(this.tabs[index]);
    this.components.map((comp) => comp.destroy());
    this.components = [];

    if (this.tabLinks.length <= index) {
      return;
    }
    this.tabLinks.forEach((tabLink) => this
      .removeClass(tabLink, 'active')
      .removeClass(tabLink.tabLink, 'active')
    );
    this.tabs.forEach((tab) => this.removeClass(tab, 'active'));
    this.addClass(this.tabLinks[index], 'active')
      .addClass(this.tabLinks[index].tabLink, 'active')
      .addClass(this.tabs[index], 'active');

    const components = this.hook('addComponents', tab.components, this);
    components.forEach((component) => this.addComponent(
      component,
      this.tabs[index],
      this.data,
      null,
      null,
      state,
    ));

    this.restoreValue();
    this.triggerChange();
    this.emit('render');
  }

  /**
   * Return all the components within all the tabs.
   */
  getAllComponents() {
    // If the validity tabs are set, then this usually means we are getting the components that have
    // triggered errors and need to iterate through these to display them.
    if (this.validityTabs && this.validityTabs.length) {
      const comps = this.validityTabs.reduce((components, component) => {
        if (component && component.getAllComponents) {
          component = component.getAllComponents();
        }
        return components.concat(component);
      }, []);
      this.validityTabs = [];
      return comps;
    }
    return super.getAllComponents();
  }

  /**
   * Checks the validity by checking all tabs validity.
   *
   * @param data
   * @param dirty
   */
  checkValidity(data, dirty) {
    if (!dirty) {
      return super.checkValidity(data, dirty);
    }
    this.components.forEach(component => component.checkValidity(data, dirty));
    if (!this.checkCondition(null, data)) {
      this.setCustomValidity('');
      return true;
    }
    const isValid = Base.prototype.checkValidity.call(this, data, dirty);
    if (!this.validityTabs || this.validityTabs.length !== this.component.components.length) {
      this.validityTabs = this.component.components.map(comp => {
        const tabComp = _.clone(comp);
        tabComp.type = 'panel';
        tabComp.internal = true;
        return this.createComponent(tabComp);
      });
    }

    return this.validityTabs.reduce((check, component) => {
      return  component.checkValidity(data, dirty) && check;
    }, isValid);
  }

  destroy() {
    const state = super.destroy() || {};
    state.currentTab = this.currentTab;
    if (this.validityTabs) {
      this.validityTabs.forEach(component => {
        component.destroy();
      });
    }
    return state;
  }

  /**
   * Make sure to include the tab on the component as it is added.
   *
   * @param component
   * @param element
   * @param data
   * @param before
   * @return {BaseComponent}
   */
  addComponent(component, element, data, before, noAdd, state) {
    component.tab = this.currentTab;
    return super.addComponent(component, element, data, before, noAdd, state);
  }

  /**
   * Only add the components for the active tab.
   */
  addComponents(element, data, options, state) {
    const { currentTab } = state && state.currentTab ? state : this;
    this.setTab(currentTab, state);
  }

  insertBefore(compElement, before) {
    if (!before || !before.component?.element?.parentElement) {
      return super.insertBefore(compElement, before);
    }
    before.component?.element?.parentElement.insertBefore(compElement, before.component.element);
  }
  attachLogic() {
    super.attachLogic();
  }
}
