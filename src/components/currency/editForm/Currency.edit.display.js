export default [
  {
    key: 'prefixSufixRow',
    type: 'columns',
    weight: 300,
    components: [
      {
        type: 'column',
        components: [
          {
            type: 'textfield',
            input: true,
            weight: 310,
            key: 'prefix',
            label: 'prefix',
            tooltip: 'Specify the prefix symbol after the component (e.g.: USD, EUR)'
          }
        ]
      },
      {
        type: 'column',
        components: [
          {
            type: 'textfield',
            input: true,
            weight: 320,
            key: 'suffix',
            label: 'suffix',
            tooltip: 'Specify the suffix symbol after the component (e.g.: USD, EUR).'
          }
        ]
      }
    ]
  }
];
