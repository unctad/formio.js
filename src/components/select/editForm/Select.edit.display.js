export default [
  {
    type: 'select',
    key: 'size',
    label: 'Field size',
    input: true,
    dataSrc: 'values',
    weight: 109,
    data: {
      values: [
        { label: 'Extra Small', value: 'xs' },
        { label: 'Small', value: 'sm' },
        { label: 'Medium', value: 'md' },
        { label: 'Large', value: 'lg' }
      ]
    }
  },
];
