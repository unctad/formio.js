import Webform from './Webform';
// Import from dist because dragula requires "global" to be defined which messes up Angular.
import dragula from 'dragula/dist/dragula';
import Tooltip from 'tooltip.js';
import Components from './components/Components';
import BuilderUtils from './utils/builder';
import {
  getComponent,
  bootstrapVersion,
  eachComponent,
  formatMoustacheSchema,
  isInsideGrid,
  isSchemaInsideGrid,
  cleanPastedSchema
} from './utils/utils';
import EventEmitter from './EventEmitter';
import NativePromise from 'native-promise-only';
import _ from 'lodash';
import baseEditForm from './components/base/Base.form';
import { transliterate } from 'transliteration';
import stringHash from 'string-hash';
require('./components/builder');

export default class WebformBuilder extends Webform {
  constructor(element, options) {
    super(element, options);
    this.builderHeight = 0;
    this.dragContainers = [];
    this.sidebarContainers = [];
    this.updateDraggable = _.debounce(this.refreshDraggable.bind(this), 200);

    // Setup the builder options.
    this.options.builder = _.defaultsDeep({}, this.options.builder, this.defaultComponents);
    this.options.enableButtons = _.defaults({}, this.options.enableButtons, {
      remove: true,
      copy: true,
      paste: true,
      edit: true,
      editJson: false,
    });
    this.options.showAdvanced = false;
    this.options.showHelp = false;
    this.options.openedPanels = false;
    this.options.pasteMode = false;

    // Turn off if explicitely said to do so...
    _.each(this.defaultComponents, (config, key) => {
      if (config === false) {
        this.options.builder[key] = false;
      }
    });

    this.builderReady = new NativePromise((resolve) => {
      this.builderReadyResolve = resolve;
    });

    this.groups = {};
    this.options.sideBarScroll = _.get(this.options, 'sideBarScroll', true);
    this.options.sideBarScrollOffset = _.get(this.options, 'sideBarScrollOffset', 0);
    this.options.hooks = this.options.hooks || {};
    this.options.hooks.addComponents = (components, parent) => {
      if (!components || (!components.length && !components.nodrop)) {
        // Return a simple alert so they know they can add something here.
        return [
          {
            type: 'htmlelement',
            internal: true,
            tag: 'div',
            className: 'drag-and-drop-alert alert',
            attrs: [
              { attr: 'id', value: `${parent.id}-placeholder` },
              { attr: 'style', value: 'text-align:center;' },
              { attr: 'role', value: 'alert' }
            ],
            content: this.t('Drag and Drop a form component')
          }
        ];
      }
      return components;
    };
    this.options.hooks.addComponent = (container, comp, parent) => {
      if (!comp || !comp.component) {
        return container;
      }
      const parentCompReadOnly = parent && parent.schemaReadOnly;
      const compReadOnly = parentCompReadOnly || comp.schemaReadOnly;

      if (!comp.noEdit && !comp.component.internal) {
        // Make sure the component position is relative so the buttons align properly.
        comp.getElement().style.position = 'relative';

        let tooltipPosition = 'top';
        if (comp.component.type === 'panel') tooltipPosition = 'top';
        if (comp.component.type === 'block') tooltipPosition = 'top';
        if (comp.component.type === 'table') tooltipPosition = 'top';
        let removeButton = null;
        let editButton = null;
        let translateButton = null;
        if (!compReadOnly) {
          removeButton = this.ce('div', {
            class: 'btn btn-xxs text-danger component-settings-button-2 component-settings-button-remove'
          }, this.getIcon('remove'));
          this.addEventListener(removeButton, 'click', () => {
            comp.deleteValue();
            this.deleteComponent(comp, true);
          });
          new Tooltip(removeButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: removeButton,
            title: this.t('Remove'),
            popperOptions: { positionFixed: true }
          });
          editButton = this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-edit'
          }, this.getIcon('cog'));
          this.addEventListener(editButton, 'click', () => {
            document.dispatchEvent(new CustomEvent('formioEditComponent', { detail: comp }));
            this.editComponent(comp);
          });
          new Tooltip(editButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: editButton,
            title: this.t('Edit'),
            popperOptions: { positionFixed: true }
          });
          translateButton = this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-edit'
          }, this.getIcon('translate'));
          this.addEventListener(translateButton, 'click', () => {
            document.dispatchEvent(new CustomEvent('formioEditComponent', {
              detail: comp,
              tab: 'translate'
            }));
            this.editComponent(comp, false, 'translate');
          });
          new Tooltip(translateButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: translateButton,
            title: this.t('Translate'),
            popperOptions: { positionFixed: true }
          });
        }
        let moveButton = null;
        if (parent && parent.type === 'datagrid') {
          moveButton = this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-edit'
          }, this.getIcon('arrows'));
          new Tooltip(moveButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: moveButton,
            title: this.t('Move'),
            popperOptions: { positionFixed: true }
          });
        }
        const minimizeButton = this.ce('div', {
          class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-minimize'
        }, this.getIcon('minus'));
        this.addEventListener(minimizeButton, 'click', () => {
          if (comp.getElement().classList.contains('minimized')) {
            comp.getElement().classList.remove('minimized');
            comp.component.minimized = false;
          }
          else {
            comp.getElement().classList.add('minimized');
            comp.component.minimized = true;
          }
          if (comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.contains('fa-minus')) {
            comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.remove('fa-minus');
            comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.add('fa-plus');
          }
          else {
            comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.add('fa-minus');
            comp.getElement().querySelector('.component-settings-button-minimize .fa').classList.remove('fa-plus');
          }

          this.updateComponent(comp);
          this.emit('saveComponent', comp);
        });
        new Tooltip(minimizeButton, {
          trigger: 'hover',
          placement: tooltipPosition,
          container: minimizeButton,
          title: this.t('Toggle size'),
          popperOptions: { positionFixed: true }
        });

        if (comp.component.minimized) {
          comp.getElement().classList.add('minimized');
          if (minimizeButton.querySelector('.fa').classList) {
            minimizeButton.querySelector('.fa').classList.remove('fa-minus');
            minimizeButton.querySelector('.fa').classList.add('fa-plus');
          }
        }

        const copyButton = this.ce('div', {
          class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-copy'
        }, this.getIcon('copy'));

        this.addEventListener(copyButton, 'click', (ev) => {
          document.dispatchEvent(new CustomEvent('formioCopyComponent', { detail: comp }));
          return this.copyComponent(comp);
        });

        new Tooltip(copyButton, {
          trigger: 'hover',
          placement: tooltipPosition,
          container: copyButton,
          title: this.t('Copy'),
          popperOptions: { positionFixed: true }
        });
        let pasteButton = null;
        let pasteWithTransformButton = null;
        if (!parentCompReadOnly) {
          pasteButton = this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-paste'
          }, this.getIcon('save'));
          const pasteTooltip = new Tooltip(pasteButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: pasteButton,
            title: this.t('Paste below'),
            popperOptions: { positionFixed: true }
          });
          this.addEventListener(pasteButton, 'click', async() => {
            pasteTooltip.hide();
            await this.pasteComponent(comp);
          });
          if (this.options.pasteWithTransformationEnabled) {
            pasteWithTransformButton = this.ce('div', {
              class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-paste'
            }, this.getIcon('transform'));
            const pasteWithTransformTooltip = new Tooltip(pasteWithTransformButton, {
              trigger: 'hover',
              placement: tooltipPosition,
              container: pasteWithTransformButton,
              title: this.t('Paste with transform below'),
              popperOptions: { positionFixed: true }
            });
            this.addEventListener(pasteWithTransformButton, 'click', async() => {
              pasteWithTransformTooltip.hide();
              await this.pasteComponent(comp, true);
            });
          }
        }
        let editJsonButton = null;
        if (!compReadOnly) {
          editJsonButton = this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-edit-json'
          }, this.getIcon('wrench'));
          this.addEventListener(editJsonButton, 'click', () => this.editComponent(comp, true));
          new Tooltip(editJsonButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: editJsonButton,
            title: this.t('Edit JSON'),
            popperOptions: { positionFixed: true }
          });
        }

        // Set in paste mode if we have an item in our clipboard.
        if (this.options.pasteMode) {
          this.addClass(this.element, 'builder-paste-mode');
        }

        // Add the edit buttons to the component.
        var fieldActions = this.ce('div', {
          class: 'component-btn-group component-configuration-group',
          'data-id': comp.component.key
        }, [
            this.options.enableButtons.edit && (comp.type === 'content' || comp.component.type === 'fieldset' || comp.component.type === 'tabs' || comp.component.type === 'table' || comp.component.type === 'columns' || comp.component.type === 'editgrid' || comp.component.type === 'datagrid' || comp.component.type === 'radio') || comp.component.type === 'container' ? minimizeButton : null,
            this.options.enableButtons.copy ? copyButton : null,
            this.options.enableButtons.paste ? pasteButton : null,
            this.options.enableButtons.paste &&  this.options.pasteWithTransformationEnabled
              ? pasteWithTransformButton : null,
            this.options.enableButtons.editJson ? editJsonButton : null,
            this.options.enableButtons.edit ? editButton : null,
            this.options.enableButtons.edit && comp.component.type !== 'table' ? translateButton : null,
            this.options.enableButtons.edit ? moveButton : null,
            this.options.enableButtons.remove ? removeButton : null
          ]);

        var children = false;
        var effectCount = Array.isArray(comp.component.effectsIds) && comp.component.effectsIds.length || comp.component.numberOfEffects;
        if (effectCount) {
          children = true;
          var hasEffect = this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-effects ${effectCount > 1 ? 'more-than-one' : ''}`
          }, 'E');
          if (!compReadOnly) {
            this.addEventListener(hasEffect, 'click', () => {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'action'
              }));
              this.editComponent(comp, false, 'determinant');
            });
          }
          new Tooltip(hasEffect, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasEffect,
            title: this.t('Effects'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasEffect);
        }

        var actionCount = Array.isArray(comp.component.actionRowIds) && comp.component.actionRowIds.length || comp.component.numberOfActions;
        if (actionCount) {
          children = true;
          var hasAction = this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-action ${actionCount > 1 ? 'more-than-one' : ''}`
          }, 'A');
          if (!compReadOnly) {
            this.addEventListener(hasAction, 'click', () => {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'action'
              }));
              this.editComponent(comp, false, 'action');
            });
          }
          new Tooltip(hasAction, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasAction,
            title: this.t('Actions'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasAction);
        }

        var determinantCount = Array.isArray(comp.component.determinantIds) && comp.component.determinantIds.length || comp.component.numberOfConditions;
        if (determinantCount) {
          children = true;
          var hasDeterminant = this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-determinant ${determinantCount > 1 ? 'more-than-one' : ''}`
          }, 'D');
          if (!compReadOnly) {
            this.addEventListener(hasDeterminant, 'click', () => {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'determinant'
              }));
              this.editComponent(comp, false, 'determinant');
            });
          }
          new Tooltip(hasDeterminant, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasDeterminant,
            title: this.t('Determinants'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasDeterminant);
        }
        var formulaCount = Array.isArray(comp.component.formulaRowIds) && comp.component.formulaRowIds.length || comp.component.numberOfFormulas;
        if (formulaCount) {
          children = true;
          var hasFormula = this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-formula ${formulaCount > 1 ? 'more-than-one' : ''}`
          }, 'F');
          if (!compReadOnly) {
            this.addEventListener(hasFormula, 'click', () => {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'formula'
              }));
              this.editComponent(comp, false, 'formula');
            });
          }
          new Tooltip(hasFormula, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasFormula,
            title: this.t('Formula'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasFormula);
        }
        var blacklistCount = Array.isArray(comp.component.blacklistRowIds) && comp.component.blacklistRowIds.length;
        if (blacklistCount) {
          children = true;
          var hasBlacklist = this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-blacklist ${blacklistCount > 1 ? 'more-than-one' : ''}`
          }, 'B');
          if (!compReadOnly) {
            this.addEventListener(hasBlacklist, 'click', () => {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'validation'
              }));
              this.editComponent(comp, false, 'validation');
            });
          }
          new Tooltip(hasBlacklist, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasBlacklist,
            title: this.t('Blacklist'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasBlacklist);
        }
        var validationCount = Array.isArray(comp.component.validationRowIds) && comp.component.validationRowIds.length;
        if (validationCount) {
          children = true;
          var hasValidation = this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-validation ${validationCount > 1 ? 'more-than-one' : ''}`
          }, 'V');
          if (!compReadOnly) {
            this.addEventListener(hasValidation, 'click', () => {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'validation'
              }));
              this.editComponent(comp, false, 'validation');
            });
          }
          new Tooltip(hasValidation, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasValidation,
            title: this.t('Validation'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasValidation);
        }
        if (Array.isArray(comp.component.copyValueFrom) && comp.component.copyValueFrom.length) {
          children = true;
          var hasCopyValueFrom = this.ce('div', {
            class: `btn btn-xxs btn-default component-configuration-button component-configuration-button-copy-value-from ${comp.component.copyValueFrom.length > 1 ? 'more-than-one' : ''}`
          }, 'C');
          if (!compReadOnly) {
            this.addEventListener(hasCopyValueFrom, 'click', () => {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'data'
              }));
              this.editComponent(comp, false, 'data');
            });
          }
          new Tooltip(hasCopyValueFrom, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasCopyValueFrom,
            title: this.t('CopyValueFrom'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasCopyValueFrom);
        }
        if (comp.component.linkedWithRequirement && comp.component.linkingRequirement || comp.component.linkedWithResult && comp.component.linkingResult) {
          children = true;
          var suffix = comp.component.linkedWithRequirement && comp.component.linkingRequirement ? 'component-configuration-button-linked-requirement' : 'component-configuration-button-linked-result';
          var hasLink = this.ce('div', {
            class: 'btn btn-xxs btn-default component-configuration-button component-configuration-button-linked '.concat(suffix)
          }, 'L');
          if (!compReadOnly) {
            this.addEventListener(hasLink, 'click', () => {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'data'
              }));
              this.editComponent(comp, false, 'data');
            });
          }
          new Tooltip(hasLink, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasLink,
            title: this.t('Linked'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasLink);
        }
        if (children) {
          fieldActions.classList.add('hasIcons');
        }

        comp.prepend(fieldActions);
      }

      if (!container.noDrop && !parentCompReadOnly) {
        this.addDragContainer(container, parent);
      }

      return container;
    };
    this.setToolbarElement();
    this.setBuilderElement();
  }

  get defaultComponents() {
    return {
      basic: {
        title: 'Basic Components',
        weight: 0,
        default: true,
      },
      advanced: {
        title: 'Advanced',
        weight: 10
      },
      layout: {
        title: 'Layout',
        weight: 20
      },
      data: {
        title: 'Data',
        weight: 30
      }
    };
  }

  scrollSidebar() {
    const newTop = (window.scrollY - this.sideBarTop) + this.options.sideBarScrollOffset;
    const shouldScroll = (newTop > 0);
    if (shouldScroll && ((newTop + this.sideBarElement.offsetHeight) < this.builderHeight)) {
      this.sideBarElement.style.marginTop = `${newTop}px`;
    }
    else if (shouldScroll && (this.sideBarElement.offsetHeight < this.builderHeight)) {
      this.sideBarElement.style.marginTop = `${this.builderHeight - this.sideBarElement.offsetHeight}px`;
    }
    else {
      this.sideBarElement.style.marginTop = '0px';
    }
  }

  setBuilderElement() {
    return this.onElement.then(() => {
      this.addClass(this.wrapper, 'row formbuilder');
      this.builderSidebar = this.ce('div', {
        class: 'col col-lg-4 col-xl-3 formcomponents',
        id: 'col1'
      });
      this.prependTo(this.builderSidebar, this.wrapper);
      this.addClass(this.element, 'formarea');
      this.element.component = this;
    });
  }

  toggleCollapsePanels() {
    const panelElements = this.element.querySelectorAll('.panel');
    const panelComponents = [];
    for (let i = 0; i < panelElements.length; i++) {
      if (panelElements[i].component) {
        panelComponents.push(panelElements[i].component);
      }
    }
    this.options.openedPanels = panelComponents.filter(c => c.component.title).every(c => !c.component.collapsed);
    this.togglePanelsTogglerIcon();
  }

  togglePanelsTogglerIcon() {
    if (this.options.openedPanels) {
      this.panelsToggler.getElementsByTagName('i')[0].classList.remove('fa-angle-down');
      this.panelsToggler.getElementsByTagName('i')[0].classList.add('fa-angle-up');
    }
    else {
      this.panelsToggler.getElementsByTagName('i')[0].classList.add('fa-angle-down');
      this.panelsToggler.getElementsByTagName('i')[0].classList.remove('fa-angle-up');
    }
  }

  togglePanels() {
    if (this.options.openedPanels) {
      this.emit('goOpenPanels', this);
    }
    else {
      this.emit('goClosePanels', this);
    }
    this.options.openedPanels = !this.options.openedPanels;
    this.togglePanelsTogglerIcon();
  }

  setToolbarElement() {
    if (this.options.builder.noToolbar) return;
    this.toolbarRow = document.getElementById('myToolbar');
    //const toolbarHtml = require('./htmltemplates/formbuilder-toolbar.html');
    const t1 = this.t('Containers');
    const t2 = this.t('Toggle blocks');
    const t3 =  this.t('Fields');
    const t4 =  this.t('Other components');
    const t5 =  this.t('Data');
    const t6 =  this.t('Custom components');
    const t7 =  this.t('Service forms');
    const toolbarHtml = `<div class="formbuilder-toolbar row"><div class="col-auto formio-component formio-component-tabs formbuilder-toolbar-fixed-container col p-0"><ul class="nav nav-tabs justify-content-between" id="formbuilder-toolbar-fixed-nav" role="tablist"><li class="nav-item"><a class="nav-link active" id="tools-containers-link" data-toggle="tab" data-target="#tools-containers" role="tab" aria-controls="tools-containers" aria-selected="true">${t1}</a></li><li class="pull-right nav-item"><a id="panelsToggler" class="nav-link"><i class="fa fa-angle-down"></i>${t2}</a></li></ul><div class="tab-content" id="formbuilder-toolbar-fixed-content"><div class="tab-pane show active" id="tools-containers" role="tabpanel" aria-labelledby="tools-containers-link"><div class="row"><div id="toolbar-layout-components" class="col"></div></div></div></div></div><div class="formio-component formio-component-tabs col formbuilder-toolbar-container p-0"><ul class="nav nav-tabs" id="formbuilder-toolbar-nav" role="tablist"><li class="nav-item"><a class="nav-link active" id="tools-fields-link" data-toggle="tab" data-target="#tools-fields" role="tab" aria-controls="tools-fields" aria-selected="true">${t3}</a></li><li class="nav-item"><a class="nav-link" id="tools-others-link" data-target="#tools-others" data-toggle="tab" role="tab" aria-controls="tools-others" aria-selected="false">${t4}</a></li><li class="nav-item"><a class="nav-link" id="tools-data-link" data-toggle="tab" data-target="#tools-data" role="tab" aria-controls="tools-data" aria-selected="false">${t5}</a></li><li class="nav-item"><a class="nav-link" id="tools-custom-link" data-toggle="tab" data-target="#tools-custom" role="tab" aria-controls="tools-custom" aria-selected="false">${t6}</a></li><li class="nav-item"><a class="nav-link" id="tools-serviceforms-link" data-toggle="tab" data-target="#tools-serviceforms" role="tab" aria-controls="tools-serviceforms" aria-selected="false">${t7}</a></li></ul><div class="tab-content" id="formbuilder-toolbar-content"><div class="tab-pane fade show active" id="tools-fields" role="tabpanel" aria-labelledby="tools-fields-link"><div class="row"><div id="toolbar-basic-components" class="col"></div></div></div><div class="tab-pane fade" id="tools-others" role="tabpanel" aria-labelledby="tools-others-link"><div class="row"><div id="toolbar-advanced-components" class="col"></div></div></div><div class="tab-pane fade" id="tools-data" role="tabpanel" aria-labelledby="tools-data-link"><div class="row"><div id="toolbar-data-components" class="col"></div></div></div><div class="tab-pane fade" id="tools-custom" role="tabpanel" aria-labelledby="tools-custom-link"><div class="row"><div id="toolbar-custom-components" class="col"></div></div></div><div class="tab-pane fade" id="tools-serviceforms" role="tabpanel" aria-labelledby="tools-custom-link"><div class="row"><div id="toolbar-serviceforms-components" class="col"></div></div></div></div></div></div>`;
    this.toolbarRow.innerHTML = toolbarHtml;
    this.groupBasic = this.toolbarRow.querySelector('#toolbar-basic-components');
    this.groupAdvanced = this.toolbarRow.querySelector('#toolbar-advanced-components');
    this.groupLayout = this.toolbarRow.querySelector('#toolbar-layout-components');
    this.groupData = this.toolbarRow.querySelector('#toolbar-data-components');
    this.groupCustom = this.toolbarRow.querySelector('#toolbar-custom-components');
    this.groupServiceForms = this.toolbarRow.querySelector('#toolbar-serviceforms-components');
    this.panelsToggler = this.toolbarRow.querySelector('#panelsToggler');

    this.panelsToggler.addEventListener('click', (event) => {
      event.preventDefault();
      this.togglePanels();
    });

    const allComponents = _.filter(_.map(Components.components, (component, type) => {
      if (!component.builderInfo || component.builderInfo.hidden) {
        return null;
      }
      component.type = type;
      return component;
    }));

    if (this.options.builder.customComponents && this.options.builder.customComponents.components) {
      Array.prototype.push.apply(allComponents, this.options.builder.customComponents.components);
    }

    const components = {};
    _.map(_.sortBy(allComponents, component => {
      return component.builderInfo.weight;
    }), (component) => {
      const builderInfo = component.builderInfo;
      builderInfo.key = component.type;
      components[builderInfo.key] = builderInfo;
    });

    _.each(components, (component) => {
      component.element = this.ce('span', {
        id: `builder-${component.key}`,
        class: `btn formcomponent drag-copy ${component.extraClass}`,
        ['aria-label']: component.title
      });
      if (component.group === 'serviceforms') {
        component.element.addEventListener('click', (ev) => {
          document.dispatchEvent(new CustomEvent('openFormForCopy', { detail: component }));
          ev.preventDefault();
        });
      }
      component.tooltip = new Tooltip(component.element, {
        trigger: 'hover',
        placement: 'bottom',
        container: component.element,
        title: this.t(component.title)
      });
      if (component.group !== 'serviceforms') {
        this.addEventListener(component.element, 'mouseout', () => {
          return component.tooltip.hide();
        });
      }
      if (component.icon) {
        component.element.appendChild(this.ce('i', {
          class: component.icon,
          'aria-hidden': 'true'
        }));
      }

      component.element.builderInfo = component;
      if (component.group === 'basic') {
        this.groupBasic.appendChild(component.element);
      }
      else if (component.group === 'advanced') {
        this.groupAdvanced.appendChild(component.element);
      }
      else if (component.group === 'layout') {
        this.groupLayout.appendChild(component.element);
      }
      else if (component.group === 'data') {
        this.groupData.appendChild(component.element);
      }
      else if (component.group === 'custom') {
        this.groupCustom.appendChild(component.element);
      }
      else if (component.group === 'serviceforms') {
        this.groupServiceForms.appendChild(component.element);
      }
    });

    this.groupBasic.component = this;
    this.groupAdvanced.component = this;
    this.groupLayout.component = this;
    this.groupData.component = this;
    this.groupCustom.component = this;
    this.groupServiceForms.component = this;
    this.updateDraggable();

    this.on('aPanelCollapsed', (comp) => {
      this.emit('saveComponent', comp);
      this.toggleCollapsePanels();
    });

    this.on('allPanelsCollapsed', (comp) => {
      this.emit('saveComponent', comp);
    });

    this.toggleCollapsePanels();
  }

  get ready() {
    return this.builderReady;
  }

  setForm(form) {
    //populate isEnabled for recaptcha form settings
    var isRecaptchaEnabled = false;
    if (form.components) {
      eachComponent(form.components, component => {
        if (isRecaptchaEnabled) {
          return;
        }
        if (component.type === 'recaptcha') {
          isRecaptchaEnabled = true;
          return false;
        }
      });
      if (isRecaptchaEnabled) {
        _.set(form, 'settings.recaptcha.isEnabled', true);
      }
      else if (_.get(form, 'settings.recaptcha.isEnabled')) {
        _.set(form, 'settings.recaptcha.isEnabled', false);
      }
    }
    this.emit('change', form);
    if (this.builderHeight && this.element) {
      this.element.style.minHeight = `${this.builderHeight}px`;
    }
    return super.setForm(form).then(retVal => {
      setTimeout(() => {
        if (this.element.style.minHeight) {
          this.element.style.minHeight = null;
        }
        this.builderHeight = this.element.offsetHeight;
      }, 200);
      return retVal;
    });
  }

  deleteComponent(component, dispatchEvent = false) {
    if (!component.parent) {
      return;
    }
    if (component.component && component.component.canBeRemoved === false) {
      return;
    }
    let remove = true;
    if ((typeof component.getComponents === 'function') && component.getComponents().length > 0) {
      const message = 'Removing this component will also remove all of its children. Are you sure you want to do this?';
      remove = window.confirm(this.t(message));
    }
    if (remove) {
      if (dispatchEvent) {
        document.dispatchEvent(new CustomEvent('formioDeleteComponent', { detail: component }));
      }
      this.emit('deleteComponentId', component.id);
      component.parent.removeComponentById(component.id);
      this.form = this.schema;
      this.emit('deleteComponent', component);
    }
    return remove;
  }

  updateComponent(component) {
    // Update the preview.
    if (this.componentPreview) {
      if (this.preview) {
        this.preview.destroy();
      }
      this.preview = Components.create(component.component, {
        preview: true,
        events: new EventEmitter({
          wildcard: false,
          maxListeners: 0
        })
      }, {}, true);
      this.preview.on('componentEdit', (comp) => {
        _.merge(component.component, comp.component);
        this.editForm.redraw();
      });
      this.preview.build();
      this.preview.isBuilt = true;
      this.componentPreview.innerHTML = '';
      this.componentPreview.appendChild(this.preview.getElement());
    }

    // Ensure this component has a key.
    if (component.isNew) {
      const namespaceKey = this.recurseNamespace(component.parent);
      if (!component.keyModified) {
        if (!namespaceKey && this.options.formName) {
          // if component is not inside any container and exists form prefix then lets add it
          component.component.key = _.camelCase(this.options.formName.concat(
            component.component.label ||
            component.component.placeholder ||
            component.component.type
          ));
        }
        else {
          component.component.key = _.camelCase(
            component.component.label ||
            component.component.placeholder ||
            component.component.type
          );
        }
        component.component.key = _.camelCase(transliterate(component.component.key));
      }

      // Set a unique key for this component.
      // BuilderUtils.uniquify(this._form, component.component);
      const ns = this.findNamespaceRoot(component.component);
     // if (!namespaceKey) {
      BuilderUtils.uniquify(this._form, component.component, _.camelCase(this.options.formName), false, false, false);
      // }
      // else {
      //   BuilderUtils.uniquify(ns, component.component);
      // }
      if (this.editForm) {
        const keyComponent = getComponent(this.editForm.components, 'key');
        if (keyComponent && keyComponent.element) {
          keyComponent.redraw();
        }
      }
    }

    // Change the "default value" field to be reflective of this component.
    if (this.defaultValueComponent) {
      const defaultValueComponentType = this.defaultValueComponent.type;
      _.assign(this.defaultValueComponent, _.omit(component.component, [
        'key',
        'label',
        'disableAddingRows',
        'disableRemovingRows',
        'disableAddingRemovingRows',
        'placeholder',
        'tooltip',
        'validate',
        'disabled',
        'hidden',
        'fields.day.required',
        'fields.month.required',
        'fields.year.required',
        'defaultValue',
        'defaultDate',
        'clearOnHide'
      ]));

      if (typeof Components.components[this.defaultValueComponent.type] !== 'function') {
        this.defaultValueComponent.type = defaultValueComponentType || this.defaultValueComponent.type;
      }

      if (typeof Components.components[this.defaultValueComponent.type] !== 'function') {
        this.defaultValueComponent.type = 'textarea';
        this.defaultValueComponent.editor = 'ace';
      }
      if (this.defaultValueComponent.components) {
        this.defaultValueComponent.components = _.cloneDeep(this.defaultValueComponent.components);
        eachComponent(this.defaultValueComponent.components, childComp => {
          if (childComp.validate) {
            delete childComp.validate;
          }
        }, true);
      }
      if (this.defaultValueComponent.customClass) {
        const customClasses = this.defaultValueComponent.customClass.split(/\s+/).filter(c => !['hide', 'hidden'].includes(c));
        this.defaultValueComponent.customClass = customClasses.join(' ');
      }
      if (this.defaultValueComponent.type === 'checkbox') {
        this.defaultValueComponent.label = 'Checked by default';
      }
      this.editForm.emit('defaultValueComponentChanged');
    }

    // Called when we update a component.
    this.emit('updateComponent', component);
  }

/**
   * When a component sets its api key, we need to check if it is unique within its namespace. Find the namespace root
   * so we can calculate this correctly.
   * @param component
   */
  findNamespaceRoot(component) {
    // First get the component with nested parents.
    const comp = getComponent((component.parent ? component.parent.components : this._form.components), component.key, true);
    const namespaceKey = this.recurseNamespace(comp);

    // If there is no key, it is the root form.
    if (!namespaceKey || this.form.key === namespaceKey) {
      return this.form.components;
    }

    // If the current component is the namespace, we don't need to find it again.
    if (namespaceKey === component.key) {
      return component.components;
    }

    // Get the namespace component so we have the original object.
    const namespaceComponent = getComponent(this.form.components, namespaceKey, true);
    return namespaceComponent.components;
  }

  recurseNamespace(component) {
    // If there is no parent, we are at the root level.
    if (!component) {
      return null;
    }

    // Some components are their own namespace.
    if (['tree'].includes(component.type) || component.tree || component.arrayTree) {
      return component.key;
    }

    // Anything else, keep going up.
    return this.recurseNamespace(component.parent);
  }

  toggleAdvancedFields() {
    if (this.dialog) {
      this.advancedFields = this.dialog.getElementsByClassName('advanced-field');
      if (this.options.showAdvanced) {
        this.dialog.advancedToggler.getElementsByTagName('i')[0].classList.remove('fa-flip-horizontal', 'text-muted');
        Array.prototype.filter.call(this.advancedFields, function(advancedField) {
          if (advancedField.parentNode.classList.contains('col')) {
            advancedField.parentNode.classList.remove('hidden');
          }
          else {
            advancedField.classList.remove('hidden');
          }
        });
        this.dialog.body.classList.add('advanced-field-enabled');
      }
      else {
        this.dialog.advancedToggler.getElementsByTagName('i')[0].classList.add('fa-flip-horizontal', 'text-muted');
        Array.prototype.filter.call(this.advancedFields, function(advancedField) {
          if (advancedField.parentNode.classList.contains('col')) {
            advancedField.parentNode.classList.add('hidden');
          }
          else {
            advancedField.classList.add('hidden');
          }
        });
        this.dialog.body.classList.remove('advanced-field-enabled');
      }
    }
  }

  toggleHelp() {
    if (this.dialog) {
      this.helpFields = this.dialog.getElementsByClassName('fa-question-circle');
      if (this.options.showHelp) {
        this.dialog.helpToggler.getElementsByTagName('i')[0].classList.remove('fa-flip-horizontal', 'text-muted');
        Array.prototype.filter.call(this.helpFields, function(helpField) {
          helpField.classList.remove('hidden');
        });
        this.dialog.body.classList.add('help-tooltip-enabled');
      }
      else {
        this.dialog.helpToggler.getElementsByTagName('i')[0].classList.add('fa-flip-horizontal', 'text-muted');
        Array.prototype.filter.call(this.helpFields, function(helpField) {
          helpField.classList.add('hidden');
        });
        this.dialog.body.classList.remove('help-tooltip-enabled');
      }
    }
  }

  /* eslint-disable max-statements */
  editComponent(component, isJsonEdit, tabKey=null) {
    this.toggleAdvancedFields();
    this.toggleHelp();
    const componentDefaultValue = component.defaultValue;
    if (_.isNil(componentDefaultValue)) {
      component.deleteValue();
    }
    else {
      component.setValue(componentDefaultValue, {
        noUpdateEvent: true
      });
    }
    const componentCopy = _.cloneDeep(component);
    let componentClass = Components.components[componentCopy.component.type];
    const isCustom = componentClass === undefined;
    //custom component should be edited as JSON
    isJsonEdit = isJsonEdit || isCustom || componentClass.useJsonEdit;
    componentClass = isCustom ? Components.components.unknown : componentClass;
    // Make sure we only have one dialog open at a time.
    if (this.dialog) {
      this.dialog.close();
    }
    this.dialog = this.createModal(componentCopy.name);
    const formioForm = this.ce('div');
    if (!this.options.noComponentPreview) {
      this.componentPreview = this.ce('div', {
        class: 'component-preview'
      });
    }
    const componentInfo = componentClass ? componentClass.builderInfo : {};

    const saveButton = this.ce('button', {
      class: 'btn btn-primary',
      style: 'margin-right: 10px;',
      'data-event': 'save-component'
    }, this.t('general.save'));
    const topSaveButton = saveButton.cloneNode(true);

    const cancelButton = this.ce('button', {
      class: 'btn btn-default',
      style: 'margin-right: 10px;'
    }, this.t('general.cancel'));
    const topCancelButton = cancelButton.cloneNode(true);

    const removeButton = this.ce('button', {
      class: 'btn btn-danger'
    }, this.t('Remove'));
    const topRemoveButton = removeButton.cloneNode(true);

    this.dialog.helpToggler = this.ce('span', {
      class: 'modal-help-toggler'
    }, [
        this.ce('i', { class: 'fa fa-toggle-large-on fa-flip-horizontal toggle__icon ml-4 mr-2' }),
        this.t('Help')
      ]);

    this.dialog.advancedToggler = this.ce('span', {
      class: 'modal-advanced-toggler'
    }, [
        this.ce('i', { class: 'fa fa-toggle-large-on fa-flip-horizontal toggle__icon ml-4 mr-2' }),
        this.t('Advanced')
      ]);

    this.addEventListener(this.dialog.helpToggler, 'click', (event) => {
      event.preventDefault();
      this.options.showHelp = !this.options.showHelp;
      this.toggleHelp();
    });

    this.addEventListener(this.dialog.advancedToggler, 'click', (event) => {
      event.preventDefault();
      this.options.showAdvanced = !this.options.showAdvanced;
      this.toggleAdvancedFields();
    });
    const componentTitle = componentCopy.component.subType ? componentCopy.component.subType : componentInfo.title;
    const componentEdit = this.ce('div', {}, [
      this.ce('div', {
        class: 'row'
      }, [
          this.ce('div', {
            class: 'col'
          }, this.ce('h1', {
            class: 'page-title'
          }, [
              component.isNew ? this.t(`Add ${componentTitle}`) : this.t(`Edit ${componentTitle}`),
              this.dialog.helpToggler,
              this.dialog.advancedToggler,
              topRemoveButton,
              topCancelButton,
              topSaveButton
            ]))
        ]),
      this.ce('div', {
        class: 'row'
      }, [
          this.ce('div', {
            class: 'col col-sm-12'
          }, formioForm),
        ])
    ]);

    // Append the settings page to the dialog body.
    this.dialog.body.appendChild(componentEdit);

    // Allow editForm overrides per component.
    const overrides = _.get(this.options, `editForm.${componentCopy.component.type}`, []);

    // Get the editform for this component.
    let editForm;
    //custom component has its own Edit Form defined
    if (isJsonEdit && !isCustom) {
      editForm = {
        'components': [
          {
            'type': 'textarea',
            'as': 'json',
            'editor': 'ace',
            'weight': 10,
            'input': true,
            'key': 'componentJson',
            'label': 'Component JSON',
            'tooltip': 'Edit the JSON for this component.'
          }
        ]
      };
    }
    else if (typeof componentClass.editForm === 'function') {
      editForm = componentClass.editForm(_.cloneDeep(overrides));
    }
    else {
      let componentEditForm = _.cloneDeep(componentClass.editForm || []);
      if (!Array.isArray(componentEditForm)) {
        componentEditForm = [];
      }
      editForm = baseEditForm([...componentEditForm, ..._.cloneDeep(overrides)]);
    }

    // Change the defaultValue component to be reflective.
    this.defaultValueComponent = getComponent(editForm.components, 'defaultValue');
    if (this.defaultValueComponent) {
      const defaultValueComponentType = this.defaultValueComponent.type;
      _.assign(this.defaultValueComponent, _.omit(componentCopy.component, [
        'key',
        'label',
        'disableAddingRows',
        'disableRemovingRows',
        'disableAddingRemovingRows',
        'placeholder',
        'tooltip',
        'validate',
        'disabled',
        'hidden',
        'fields.day.required',
        'fields.month.required',
        'fields.year.required',
        'defaultValue',
        'defaultDate',
        'clearOnHide'
      ]));
      if (Array.isArray(this.defaultValueComponent.customClasses) && this.defaultValueComponent.customClasses.length > 0) {
        this.defaultValueComponent.customClass = this.defaultValueComponent.customClasses.filter(c => !['hide', 'hidden'].includes(c)).join(' ');
        delete this.defaultValueComponent.customClasses;
      }
      else if (this.defaultValueComponent.customClass) {
        const customClasses = this.defaultValueComponent.customClass.split(/\s+/).filter(c => !['hide', 'hidden'].includes(c));
        this.defaultValueComponent.customClass = customClasses.join(' ');
      }
      if (typeof Components.components[this.defaultValueComponent.type] !== 'function') {
        this.defaultValueComponent.type = defaultValueComponentType || this.defaultValueComponent.type;
      }
      if (typeof Components.components[this.defaultValueComponent.type] !== 'function') {
        this.defaultValueComponent.type = 'textarea';
        this.defaultValueComponent.editor = 'ace';
      }
      if (this.defaultValueComponent.type === 'checkbox') {
        this.defaultValueComponent.label = 'Checked by default';
      }
      if (this.defaultValueComponent.components) {
        this.defaultValueComponent.components = _.cloneDeep(this.defaultValueComponent.components);
        eachComponent(this.defaultValueComponent.components, childComp => {
          if (childComp.validate) {
            delete childComp.validate;
          }
        }, true);
      }
    }

    // Create the form instance.
    const editFormOptions = _.get(this, 'options.editForm', {});
    this.editForm = new Webform(formioForm, {
      language: this.options.language,
      ...editFormOptions
    });

    if (component.isNew) this.editForm.element.classList.add('is-new');

    // Set the form to the edit form.
    this.editForm.form = editForm;

    // Pass along the form being edited.
    this.editForm.editForm = this._form;
    this.editForm.editComponent = component;

    // Update the preview with this component.
    this.updateComponent(componentCopy);

    // Register for when the edit form changes.
    this.editForm.on('change', (event) => {
      this.toggleAdvancedFields();
      this.toggleHelp();
      if (event.changed) {
        // See if this is a manually modified key. Treat JSON edited component keys as manually modified
        if ((event.changed.component && (event.changed.component.key === 'key')) || isJsonEdit) {
          componentCopy.keyModified = true;
        }

        // Set the component JSON to the new data.
        var editFormData = this.editForm.getValue().data;
        //for custom component use value in 'componentJson' field as JSON of component
        if ((editFormData.type === 'custom' || isJsonEdit) && editFormData.componentJson) {
          componentCopy.component = editFormData.componentJson;
        }
        else {
          componentCopy.component = editFormData;
        }

        // Update the component.
        this.updateComponent(componentCopy);
      }
    });

    // Modify the component information in the edit form.
    this.editForm.formReady.then(() => {
      //for custom component populate component setting with component JSON
      if (isJsonEdit) {
        this.editForm.setValue({
          data: {
            componentJson: _.cloneDeep(componentCopy.component)
          }
        });
      }
      else {
        this.editForm.setValue({ data: componentCopy.component });
        if (tabKey) {
          const tabsComp = this.editForm.getComponent('tabs');
          if (tabsComp && tabsComp.component && Array.isArray(tabsComp.component.components)) {
            // eslint-disable-next-line max-depth
            for (let i = 0; i < tabsComp.component.components.length; i++) {
              // eslint-disable-next-line max-depth
              if (tabsComp.component.components[i].key === tabKey) {
                tabsComp.setTab(i);
                break;
              }
            }
          }
        }
      }
      this.toggleAdvancedFields();
      this.toggleHelp();
      if (this.defaultValueComponent) {
        this.editForm.emit('defaultValueComponentChanged');
      }
    });

    this.addEventListener(topCancelButton, 'click', (event) => {
      event.preventDefault();
      this.emit('cancelComponent', component);
      this.dialog.close();
    });

    this.addEventListener(cancelButton, 'click', (event) => {
      event.preventDefault();
      this.emit('cancelComponent', component);
      this.dialog.close();
    });

    this.addEventListener(topRemoveButton, 'click', (event) => {
      event.preventDefault();
      component.deleteValue();
      this.deleteComponent(component);
      this.dialog.close();
    });

    this.addEventListener(removeButton, 'click', (event) => {
      event.preventDefault();
      component.deleteValue();
      this.deleteComponent(component);
      this.dialog.close();
    });

    this.addEventListener(saveButton, 'click', (event) => {
      event.preventDefault();
      if (!this.editForm.checkValidity(this.editForm.data, true)) {
       console.log('Form data is not valid! Cancelling saving!', this.editForm.data);
       const errors = this.editForm.errors;
       console.log('Validation errors', errors);
       this.emit('validationErrors', errors, component);
       return;
      }
      const originalComponent = component.schema;
      component.isNew = false;
      component.deleteValue();
      //for JSON Edit use value in 'componentJson' field as JSON of component
      if (isJsonEdit) {
        component.component = this.editForm.data.componentJson;
      }
      else {
        component.component = componentCopy.component;
      }
      if (component.dragEvents && component.dragEvents.onSave) {
        component.dragEvents.onSave(component);
      }
      this.form = this.schema;
      this.emit('saveElement', this.editForm.data);
      this.emit('saveComponent', component, originalComponent);
      this.dialog.close();
    });

    this.addEventListener(topSaveButton, 'click', (event) => {
      event.preventDefault();
      if (!this.editForm.checkValidity(this.editForm.data, true)) {
        console.log('Form data is not valid! Cancelling saving!', this.editForm.data);
        const errors = this.editForm.errors;
        console.log('Validation errors', errors);
        this.emit('validationErrors', errors, component);
        return;
      }
      const originalComponent = component.schema;
      component.isNew = false;
      component.deleteValue();
      //for JSON Edit use value in 'componentJson' field as JSON of component
      if (isJsonEdit) {
        component.component = this.editForm.data.componentJson;
      }
      else {
        component.component = componentCopy.component;
      }
      if (component.dragEvents && component.dragEvents.onSave) {
        component.dragEvents.onSave(component);
      }
      this.form = this.schema;
      this.emit('saveElement', this.editForm.data);
      this.emit('saveComponent', component, originalComponent);
      this.dialog.close();
    });

    this.addEventListener(this.dialog, 'close', () => {
      this.editForm.destroy(true);
      if (this.componentPreview) {
        this.preview.destroy(true);
      }
      this.dialog.remove();
      if (component.isNew) {
        this.deleteComponent(component);
      }
    });

    // Called when we edit a component.
    this.emit('editComponent', component);
  }
  /* eslint-enable max-statements */

  /**
   * Creates copy of component schema and stores it under clipboard.
   * @param {Component} component
   * @return {*}
   */
  copyComponent(component) {
    this.addClass(this.element, 'builder-paste-mode');
    const copy = { 'formio.clipboard': {
      component: _.cloneDeep(component.schema),
      origin: this.options.BASE_URL,
      serviceId: this.options.serviceId
    } };

    navigator.clipboard.writeText(JSON.stringify(copy)).then((message) => {
      this.emit('notify', 'Component schema added to clipboard!', component);
    } );
  }

  /**
   * Paste copied component after the current component.
   * @param {Component} component
   * @return {*}
   */
  async pasteComponent(component, transform = false) {
    let data = await navigator.clipboard.readText();
    try {
      data = JSON.parse(data);
      const pastedObject = data && data['formio.clipboard'] && data['formio.clipboard'];
      if (!pastedObject || !pastedObject.component) {
        this.emit('validationErrors', [{ message: 'Paste failed! Clipboard does not contain formio component' }], component);
        return;
      }
      if (transform && (pastedObject.serviceId !== this.options.serviceId || pastedObject.origin !== this.options.BASE_URL)) {
        this.emit('validationErrors', [{ message: 'Paste failed! Paste with transformation is allowed only when copying component from the same service' }], component);
        return;
      }
      this.removeClass(this.element, 'builder-paste-mode');
      const schema = pastedObject.component;
      let idMapping = false;

      cleanPastedSchema(schema, transform);
      formatMoustacheSchema(schema, isInsideGrid(component));
      navigator.clipboard.writeText('').then((message) => {});
      this.options.pasteMode = false;
      if (transform) {
        const tabItemsWithDeterminantIds = [];
        eachComponent([schema], (comp) => {
          if ((!comp.type || comp.type === 'tab') && Array.isArray(comp.components) && comp.condition > 0) {
            tabItemsWithDeterminantIds.push(comp.id);
          }
        }, true);
        idMapping = {};
        if (tabItemsWithDeterminantIds.length > 0) {
          const formDeterminants = await Promise.all(tabItemsWithDeterminantIds
            .map(id => this.options.methods.generateFormDeterminantOutOfOtherRelation('TabItem', id)));
          formDeterminants.forEach((fd, index) => {
            idMapping[tabItemsWithDeterminantIds[index]] = {
              id: fd.id,
              determinantIds: this.options.methods.findAllPossibleDeterminantIds(fd.jsonDeterminant)
            };
          });
        }
      }

      const keyMap = BuilderUtils.uniquify(this._form, schema, _.camelCase(this.options.formName), true, transform, idMapping);

      if (transform) {
        const newDeterminantIds = await this.options.methods.generateDeterminantForNewKeys(BuilderUtils.extractNecessaryProperties(schema), keyMap);
        eachComponent([schema], (comp) => {
          if (comp.formDeterminantId && newDeterminantIds[comp.formDeterminantId]) {
            comp.formDeterminantId = newDeterminantIds[comp.formDeterminantId];
          }
          if (comp.determinantIds && Array.isArray(comp.determinantIds)) {
            comp.determinantIds.forEach((id, index) => {
              if (newDeterminantIds[id]) comp.determinantIds[index] = newDeterminantIds[id];
            });
          }
        }, true);
      }

      // TODO following is needed for TOBE-11818
      //         const prePaste = {
      //           keyMap: BuilderUtils.uniquify(this._form, schema, _.camelCase(this.options.formName), true),
      //           origin:  data['formio.clipboard'].origin,
      //           serviceId:  data['formio.clipboard'].serviceId,
      //           formName:  _.camelCase(this.options.formName),
      //           pasteAfter: component.key,
      //           pastedComponent: schema
      //         };

      // If this is an empty "nested" component, and it is empty, then paste the component inside this component.
      if ((typeof component.addComponent === 'function') && !component.components.length) {
        if (schema.type === 'form' || schema.type === 'wizard') {
          // eslint-disable-next-line max-depth
          if (Array.isArray(schema.components)) {
            // eslint-disable-next-line max-depth
            for (const subSchema of schema.components) {
              component.addComponent(subSchema);
            }
          }
        }
        else {
          component.addComponent(schema);
        }
      }
      else {
        if (schema.type === 'form' || schema.type === 'wizard') {
          // eslint-disable-next-line max-depth
          if (Array.isArray(schema.components)) {
            // eslint-disable-next-line max-depth
            for (let i = schema.components.length - 1; i >= 0; i--) {
              component.parent.addComponent(schema.components[i], false, false, component.element.nextSibling);
            }
          }
        }
        else {
          component.parent.addComponent(schema, false, false, component.element.nextSibling);
        }
      }
      this.form = this.schema;
      this.emit('pasteComponent', component);
    }
    catch (e) {
      console.error('error happened', e);
      this.emit('validationErrors', [{ message: 'Paste failed! Please try to copy again' }], component);
    }
  }

  destroy() {
    const state = super.destroy();
    if (this.dragula) {
      this.dragula.destroy();
    }
    return state;
  }

  /**
   * Insert an element in the weight order.
   *
   * @param info
   * @param items
   * @param element
   * @param container
   */
  insertInOrder(info, items, element, container) {
    // Determine where this item should be added.
    let beforeWeight = 0;
    let before = null;
    _.each(items, (itemInfo) => {
      if (
        (info.key !== itemInfo.key) &&
        (info.weight < itemInfo.weight) &&
        (!beforeWeight || (itemInfo.weight < beforeWeight))
      ) {
        before = itemInfo.element;
        beforeWeight = itemInfo.weight;
      }
    });

    if (before) {
      try {
        container.insertBefore(element, before);
      }
      catch (err) {
        container.appendChild(element);
      }
    }
    else {
      container.appendChild(element);
    }
  }

  getFieldIcon(type) {
    if (type) {
      switch (type.toLowerCase()) {
          case 'panel':
          case 'block':
              return 'far fa-window';
          case 'editgrid':
          case 'datagrid':
              return 'far fa-th';
          case 'table':
              return 'far fa-table';
          case null:
          default:
              return false;
      }
    }
    else {
        return false;
    }
  }

  addBuilderGroup(info, container, fromAsyncContext = false, path=null) {
    if (!info || !info.key) {
      console.warn('Invalid Group Provided.');
      return;
    }
    if (!path) {
      path = '';
    }
    const currentPath = path ? `${path}.${info.key}` : info.key;
    info.uKey = stringHash(currentPath);
    info = _.clone(info);

    let ico = '';
    if (this.getFieldIcon(info.type)) {
      ico = this.ce('i',
      { class: `${this.getFieldIcon(info.type)} branch-bloc-icon mr-2` }, '');
      if (info.disabled) {
        new Tooltip(ico, {
          trigger: 'hover',
          placement: 'left',
          container: ico,
          title: this.t('Not in roles flow'),
          popperOptions: { positionFixed: true }
        });
      }
    }

    let moustach = '';
    if (info.hasMoustach) {
      moustach = this.ce('i',
        { class : 'working-title' }, '');
        new Tooltip(moustach, {
          trigger: 'hover',
          placement: 'right',
          container: 'body',
          title: info.name,
          popperOptions: { positionFixed: true }
        });
    }
    const groupAnchor = this.ce('button', {
      class: 'btn btn-block builder-group-button no-drag',
      'type': 'button',
      'data-toggle': 'collapse',
      'data-parent': `#${container.id}`,
      'data-target': `#group-${info.uKey}`
    }, [
      ico,
      this.text(info.title),
      moustach
    ]);

    var initiateCopy = '';
    if (!info.noCopy && info.type !== 'form' && info.key !== 'allFields') {
      initiateCopy = this.ce('span', {
        class: 'copyComponent fa fa-copy',
      });
      this.addEventListener(initiateCopy, 'click', () => {
        document.dispatchEvent(new CustomEvent('formioCopyComponent', { detail: info }));
        return this.copyComponent(info);
      }, true);
    }

    // Add a listener when it is clicked.
    if (!bootstrapVersion(this.options)) {
      this.addEventListener(groupAnchor, 'click', (event) => {
        event.preventDefault();
        const clickedGroupId = event.target.getAttribute('data-target').replace('#group-', '');
        if (this.groups[clickedGroupId]) {
          const clickedGroup = this.groups[clickedGroupId];
          const wasIn = this.hasClass(clickedGroup.panel, 'in');
          if (wasIn) {
            this.removeClass(clickedGroup.panel, 'in');
            this.removeClass(clickedGroup.panel, 'show');
          }
          else {
            let previousGroup = null;
            let parent = clickedGroup;
            while (parent) {
              this.addClass(parent.panel, 'in');
              this.addClass(parent.panel, 'show');
              // eslint-disable-next-line max-depth
              if (parent.groups) {
                // eslint-disable-next-line max-depth
                for (const subGroup of Object.values(parent.groups)) {
                  // eslint-disable-next-line max-depth
                  if (previousGroup && previousGroup !== subGroup) {
                    this.removeClass(subGroup.panel, 'in');
                    this.removeClass(subGroup.panel, 'show');
                  }
                }
              }
              previousGroup = parent;
              parent = parent.parent;
            }
          }
          // Match the form builder height to the sidebar.
          this.element.style.minHeight = `${this.builderSidebar.offsetHeight}px`;
          this.scrollSidebar();
          event.stopPropagation();
        }
      }, true);
    }

    let panelClass = 'card panel panel-default form-builder-panel no-drag';
    if (info.disabled) {
      panelClass += ' disabled';
    }

    info.element = this.ce('div', {
      class: panelClass,
      id: `group-panel-${_.camelCase(info.key)}`
    }, [
      this.ce('div', {
        class: 'card-header panel-heading form-builder-group-header no-drag'
      }, [
        this.ce('h5', {
          class: 'mb-0 panel-title'
        }, [groupAnchor, initiateCopy])
      ])
    ]);
    info.body = this.ce('div', {
      id: `group-container-${_.camelCase(info.key)}`,
      class: 'card-body panel-body no-drop'
    });

    // Add this group body to the drag containers.
    this.sidebarContainers.push(info.body);

    let groupBodyClass = 'panel-collapse collapse';
    if (info.default) {
      switch (bootstrapVersion(this.options)) {
        case 4:
          groupBodyClass += ' show';
          break;
        case 3:
          groupBodyClass += ' in';
          break;
        default:
          groupBodyClass += ' in show';
          break;
      }
    }

    info.panel = this.ce('div', {
      class: groupBodyClass,
      'data-parent': `#${container.id}`,
      id: `group-${_.camelCase(info.key)}`
    }, info.body);

    info.element.appendChild(info.panel);
    this.groups[info.uKey] = info;
    this.insertInOrder(info, this.groups, info.element, container);

    const cb = (asyncContext = false) => {
      // Now see if this group has subgroups.
      if (info.groups) {
        _.each(info.groups, (subInfo, subGroup) => {
          subInfo.key = subGroup;
          subInfo.parent = info;
          this.addBuilderGroup(subInfo, info.body, asyncContext, currentPath);
        });
      }
      if (asyncContext) {
        _.each(info.components, (comp, key) => {
          if (comp && !(comp.components && comp.components.length)) {
            this.addBuilderComponent(comp, info, currentPath);
          }
        });

        if ((!info.components || !Object.keys(info.components).length)
          && (!info.groups || !Object.keys(info.groups).length)) {
            this.addBuilderComponent({
              key: 'empty',
              title: this.t('No choices to choose from'),
              noOptions: true,
              noCopy: true
            }, info, currentPath);
        }
      }
    };

    if (info.loadOnDemand) {
      const loadComponents = (loadedInfo) => {
        info.alreadyLoaded = true;
        Object.assign(info, loadedInfo);
        this.groups[info.uKey] = info;
        // eslint-disable-next-line callback-return
        cb(true);
        groupAnchor.classList.remove('preload');
        this.updateDraggable();
      };
      this.addEventListener(groupAnchor, 'click', (event) => {
        event.preventDefault();
        if (info.alreadyLoaded) return;
        groupAnchor.classList.add('preload');
        return info.loadOnDemand(loadComponents);
      }, true);
    }
    else {
      return cb(fromAsyncContext);
    }
  }

  addBuilderComponentInfo(component) {
    if (!component || !component.group || !this.groups[component.group]) {
      return;
    }

    component = _.clone(component);
    const groupInfo = this.groups[component.group];
    if (!groupInfo.components) {
      groupInfo.components = {};
    }
    if (!groupInfo.components.hasOwnProperty(component.key)) {
      groupInfo.components[component.key] = component;
    }
    return component;
  }

  addBuilderComponent(component, group, path=null) {
    if (!component) {
      return;
    }
    if (!group && component.group && this.groups[component.group]) {
      group = this.groups[component.group];
    }
    if (!group || component.groups && Object.keys(component.groups).length > 1) {
      return;
    }
    if (!path) {
      path = '';
    }
    var compClass = `btn btn-primary btn-xs btn-block formcomponent no-drag ${component.type}`;
    if (component.disabled) {
      compClass += ' disabled';
    }
    const currentPath = path ? `${path}.${component.key}` : component.key;
    component.uKey = stringHash(currentPath);
    component.element = this.ce('span', {
      id: `builder-${component.uKey}`,
      class: compClass
    });
    if (component.icon) {
      var ico = this.ce('i', {
        class: `${component.icon} mr2`
      });
      if (component.disabled) {
        new Tooltip(ico, {
          trigger: 'hover',
          placement: 'left',
          container: ico,
          title: this.t('Not in roles flow'),
          popperOptions: { positionFixed: true }
        });
      }
      component.element.appendChild(ico);
    }

    var copyMoustash = this.ce('span', {
      class: component.noOptions ? '' : 'copyMoustach far fa-brackets-curly',
    });
    component.element.appendChild(copyMoustash);
    if (!component.noOptions) {
      this.addEventListener(copyMoustash, 'click', function() {
        var variable = `${isSchemaInsideGrid(group) ? 'row' : 'data'}.${component.key}`;
        if (component.type === 'select') {
          variable += '.value';
        }
        if (component.key.endsWith('_child_value') || component.key.endsWith('_child_key')) {
          variable = variable.replace(/_child_(?!.*_child_)/, '.');
        }
        navigator.clipboard.writeText(`{{${variable}}}`);
      }, true);
    }
    if (!component.noCopy) {
      var initiateCopy = this.ce('span', {
        class: 'copyComponent fa fa-copy',
      });
      new Tooltip(initiateCopy, {
        trigger: 'hover',
        placement: 'top',
        container: initiateCopy,
        title: this.t('Copy'),
        popperOptions: { positionFixed: true }
      });
      component.element.appendChild(initiateCopy);
      this.addEventListener(initiateCopy, 'click', () => {
        document.dispatchEvent(new CustomEvent('formioCopyComponent', { detail: component }));
        return this.copyComponent(component);
      }, true);
    }
    component.element.builderInfo = component;

    let moustach = '';
    if (component.workingLabel) {
      moustach = this.ce('i',
        { class : 'working-title' }, '');
        new Tooltip(moustach, {
          trigger: 'hover',
          placement: 'right',
          container: 'body',
          title: component.name,
          popperOptions: { positionFixed: true }
        });
    }

    var elementLabel = this.ce('span', {
      class: 'btn-label'
    }, [this.text(component.title), moustach]);
    component.element.appendChild(elementLabel);
    this.insertInOrder(component, group.components, component.element, group.body);
    return component;
  }

  addBuilderButton(info, container) {
    let button;
    if (!info.uKey) {
      info.uKey = stringHash(info.key);
    }
    info.element = this.ce('div', {
      style: 'margin: 5px 0;'
    },
      button = this.ce('span', {
        class: `btn btn-block ${info.style || 'btn-default'}`,
      }, info.title)
    );
    // Make sure it persists across refreshes.
    this.addEventListener(button, 'click', () => this.emit(info.event), true);
    this.groups[info.uKey] = info;
    this.insertInOrder(info, this.groups, info.element, container);
  }

  buildSidebar() {
    // Do not rebuild the sidebar.
    if (this.sideBarElement || this.options.builder.noSideBar) {
      return;
    }
    this.groups = {};
    this.sidebarContainers = [];
    this.sideBarElement = this.ce('div', {
      id: `builder-sidebar-${this.id}`,
      class: 'accordion panel-group'
    });

    this.searchInpuSidebar = this.ce('input', { class: 'form-control', placeholder: this.t('Search') });
    this.sideBarElement.appendChild(this.searchInpuSidebar);
    this.addEventListener(this.searchInpuSidebar, 'input', (event) => {
      event.stopPropagation();
      this.searchSidebarGroups(this.searchInpuSidebar.value.toLowerCase());
    }, true);
    this.addEventListener(this.searchInpuSidebar, 'click', (event) => {
      event.preventDefault();
      event.stopPropagation();
    }, true);
    // Add the groups.
    _.each(this.options.builder, (info, group) => {
      if (info) {
        info.key = group;
        if (info.type === 'button') {
          this.addBuilderButton(info, this.sideBarElement);
        }
        else {
          this.addBuilderGroup(info, this.sideBarElement);
        }
      }
    });

    // Get all of the components builder info grouped and sorted.
    const components = {};
    const allComponents = _.filter(_.map(Components.components, (component, type) => {
      if (!component.builderInfo) {
        return null;
      }
      component.type = type;
      return component;
    }));
    _.map(_.sortBy(allComponents, component => {
      return component.builderInfo.weight;
    }), (component) => {
      const builderInfo = component.builderInfo;
      builderInfo.key = component.type;
      components[builderInfo.key] = builderInfo;
      this.addBuilderComponentInfo(builderInfo);
    });

    // Add the components in each group.
    _.each(this.groups, (info) => {
      if (info.loadOnDemand) return;
      _.each(info.components, (comp, key) => {
        if (comp) {
          this.addBuilderComponent(comp === true ? components[key] : comp, info);
        }
      });
    });

    // Add the new sidebar element.
    this.builderSidebar.appendChild(this.sideBarElement);
    this.updateDraggable();
    this.sideBarTop = this.sideBarElement.getBoundingClientRect().top + window.scrollY;
  }

  getParentElement(element) {
    let containerComponent = element;
    do {
      containerComponent = containerComponent.parentNode;
    } while (containerComponent && !containerComponent.component);
    return containerComponent;
  }

  addDragContainer(element, component, dragEvents) {
    _.remove(this.dragContainers, (container) => (element.id && (element.id === container.id)));
    element.component = component;
    if (dragEvents) {
      element.dragEvents = dragEvents;
    }
    this.addClass(element, 'drag-container');
    if (!element.id) {
      element.id = `builder-element-${component.id}`;
    }
    this.dragContainers.push(element);
    this.updateDraggable();
  }

  searchSidebarGroups(text, groups=null) {
    let someMatched = false;
    const genericComps = Array.isArray(groups) ? groups : Object.values(groups || this.groups);
    for (const genericComp of genericComps) {
      if (!groups && genericComp.parent) {
        continue;
      }
      let visible = true;
      if (text) {
        let match = (genericComp.title || genericComp.name || genericComp.label || '').toLowerCase().includes(text);
        if (genericComp.groups) {
          // eslint-disable-next-line max-depth
          if (this.searchSidebarGroups(text, genericComp.groups)) {
            match = true;
          }
        }
        if (genericComp.components) {
          // eslint-disable-next-line max-depth
          if (this.searchSidebarGroups(text, genericComp.components)) {
            match = true;
          }
        }
        someMatched = someMatched || match;
        if (!match) {
          visible = false;
        }
      }
      else {
        if (genericComp.groups) {
          this.searchSidebarGroups(text, genericComp.groups);
        }
        if (genericComp.components) {
          this.searchSidebarGroups(text, genericComp.components);
        }
        someMatched = true;
      }
      const element = genericComp.element || this.groups[genericComp.uKey]?.element;
      if (element) {
        if (visible) {
          // eslint-disable-next-line max-depth
          if (element.classList.contains('d-none')) {
            element.classList.remove('d-none');
          }
        }
        else if (!element.classList.contains('d-none')) {
          element.classList.add('d-none');
        }
      }
    }
    return someMatched;
  }

  clear() {
    this.dragContainers = [];
    return super.clear();
  }

  addComponentTo(schema, parent, element, sibling, after) {
    const component = parent.addComponent(
      schema,
      element,
      parent.data,
      sibling
    );

    if (after) {
      after(component);
    }

    // Get path to the component in the parent component.
    let path = 'components';
    switch (component.parent.type) {
      case 'table':
        path = `rows[${component.tableRow}][${component.tableColumn}].components`;
        break;
      case 'columns':
        path = `columns[${component.column}].components`;
        break;
      case 'tabs':
        path = `components[${component.tab}].components`;
        break;
    }
    // Index within container
    const index = _.findIndex(_.get(component.parent.schema, path), { key: component.component.key }) || 0;
    this.emit('addComponent', component, path, index);
    return component;
  }

  /* eslint-disable  max-statements */
  onDrop(element, target, source, sibling) {
    if (!element || !element.id) {
      console.warn('No element.id defined for dropping');
      return;
    }
    const builderElement = source.querySelector(`#${element.id}`);
    const newParent = this.getParentElement(element);
    if (!newParent || !newParent.component) {
      return console.warn('Could not find parent component.');
    }

    // Remove any instances of the placeholder.
    let placeholder = document.getElementById(`${newParent.component.id}-placeholder`);
    if (placeholder) {
      placeholder = placeholder.parentNode;
      placeholder.parentNode.removeChild(placeholder);
    }

    // If the sibling is the placeholder, then set it to null.
    if (sibling === placeholder) {
      sibling = null;
    }

    // Make this element go before the submit button if it is still on the builder.
    if (!sibling && this.submitButton && newParent.contains(this.submitButton.element)) {
      sibling = this.submitButton.element;
    }

    // If this is a new component, it will come from the builderElement
    if (
      builderElement &&
      builderElement.builderInfo &&
      !builderElement.builderInfo.schema && builderElement.builderInfo.builderInfo
    ) {
      builderElement.builderInfo.schema = builderElement.builderInfo.builderInfo.schema;
    }

    if (
      builderElement &&
      builderElement.builderInfo &&
      builderElement.builderInfo.schema
    ) {
      const componentSchema = _.clone(builderElement.builderInfo.schema);
      if (this.options.defaultComponents) {
        if (this.options.defaultComponents['*']) {
          Object.assign(componentSchema, _.clone(this.options.defaultComponents['*']));
        }
        if (componentSchema.type && this.options.defaultComponents[componentSchema.type]) {
          Object.assign(componentSchema, _.clone(this.options.defaultComponents[componentSchema.type]));
        }
      }
      if (target.dragEvents && target.dragEvents.onDrop) {
        target.dragEvents.onDrop(element, target, source, sibling, componentSchema);
      }

      // Add the new component.
      const component = this.addComponentTo(componentSchema, newParent.component, newParent, sibling, (comp) => {
        // Set that this is a new component.
        comp.isNew = true;

        // Pass along the save event.
        if (target.dragEvents) {
          comp.dragEvents = target.dragEvents;
        }
      });

      // Edit the component.
      this.editComponent(component);

      // Remove the element.
      if (target.contains(element)) {
        target.removeChild(element);
      }
    }
    // Check to see if this is a moved component.
    else if (element.component) {
      const componentSchema = element.component.schema;
      if (target.dragEvents && target.dragEvents.onDrop) {
        target.dragEvents.onDrop(element, target, source, sibling, componentSchema);
      }

      // Remove the component from its parent.
      if (element.component.parent) {
        this.emit('deleteComponentForMoving', element.component);
        element.component.parent.removeComponent(element.component);
      }

      // Add the new component.
      const component = this.addComponentTo(
        componentSchema,
        newParent.component,
        newParent,
        sibling
      );
      if (target.dragEvents && target.dragEvents.onSave) {
        target.dragEvents.onSave(component);
      }

      // Refresh the form.
      if (target.component && target.component.root && target.component.root !== this) {
        target.component.root.form = target.component.root.schema;
      }
      this.form = this.schema;
      this.emit('saveComponent', component);
    }
  }
  /* eslint-enable  max-statements */

  /**
   * Adds a submit button if there are no components.
   */
  addSubmitButton() {
    if (!this.getComponents().length) {
      this.submitButton = this.addComponent({
        type: 'button',
        label: this.t('Submit'),
        key: 'submit',
        size: 'md',
        block: false,
        action: 'submit',
        disableOnInvalid: true,
        theme: 'primary'
      });
    }
  }

  refreshDraggable() {
    if (this.dragula) {
      this.dragula.destroy();
    }
    let dragables = this.sidebarContainers.concat(this.dragContainers).concat(this.groupBasic).concat(this.groupAdvanced).concat(this.groupLayout).concat(this.groupData).concat(this.groupCustom).concat(this.groupServiceForms);
    if (this.options.externalDragables) {
      dragables = dragables.concat(this.options.externalDragables);
    }
    const that = this;
    dragables.forEach((dragable) => {
      if (!dragable) return;
      dragable.addEventListener('click', (ev) => {
        that.cancelDragAndDrop = true;
      });
    });
    this.dragula = dragula(dragables, {
        moves(el) {
          if (el.classList.contains('no-drag')) {
            return false;
          }
          setTimeout(() => {
            if (that.cancelDragAndDrop) {
              that.cancelDragAndDrop = false;
              return;
            }
            document.dispatchEvent(new CustomEvent('formioFormBuilderDragStarted', {
              detail: that
            }));
          }, 300);

          return true;
        },
        copy(el) {
          return el.classList.contains('drag-copy');
        },
        accepts(el, target) {
          return !el.contains(target) && !target.classList.contains('no-drop');
        }
      }).on('drop', (element, target, source, sibling) => {
        var result = this.onDrop(element, target, source, sibling);
        document.dispatchEvent(new CustomEvent('formioFormBuilderDragEnded', { detail: this }));
        return result;
      });

    // If there are no components, then we need to add a default submit button.
    //this.addSubmitButton();
    this.builderReadyResolve();
  }

  build(state) {
    const cb = () => {
      this.buildSidebar();
      super.build(state);
      this.updateDraggable();
      this.formReadyResolve();
    };

    navigator.permissions.query({ name: 'clipboard-read' }).then(permissionStatus => {
        // Check if permission is granted
      this.options.enableButtons.copy = permissionStatus.state === 'granted';
        permissionStatus.onchange = () => {
          location.reload();
        };

      return navigator.clipboard.readText().then((clipboard) => {
        try {
          clipboard = JSON.parse(clipboard);
          this.options.pasteMode = clipboard && clipboard.hasOwnProperty('formio.clipboard');
        }
        catch (e) {
          this.options.pasteMode = false;
        }
        cb();
      }, (error) => {
        this.options.pasteMode = false;
        cb();
      });
    });
  }
}
