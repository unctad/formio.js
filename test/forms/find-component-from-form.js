import { expect } from 'chai';
import {
  syriaForm,
  nestedConditionalForm
} from '../fixtures';

import { getComponent } from '../../src/utils/formUtils';

export default {
  title: 'Conditional Nested Form Load',
  form: nestedConditionalForm,
  tests: {
    'does it find easy component from form'(form, done) {
      const component = getComponent(nestedConditionalForm.components, 'radio');
      expect(component).to.be.an('object');
      done();
    },
    'does it find nested component from form'(form, done) {
      const component = getComponent(syriaForm, 'applicantCopyOfNationalIdOrPassport');
      expect(component).to.be.an('object');
      done();
    }
  }
};
